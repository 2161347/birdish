﻿using SQLite4Unity3d;
using UnityEngine;
using System;
using System.Collections;
using UnityEditor;
using System.Linq;
#if !UNITY_EDITOR
using System.Collections;
using System.IO;
#endif
using System.Collections.Generic;

public class DataService
{

    private SQLiteConnection _connection;

    public DataService(string DatabaseName)
    {

#if UNITY_EDITOR
        var dbPath = string.Format(@"Assets/StreamingAssets/{0}", DatabaseName);
#else
        // check if file exists in Application.persistentDataPath
        var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);

        if (!File.Exists(filepath))
        {
            
            // if it doesn't ->
            // open StreamingAssets directory and load the db ->

#if UNITY_ANDROID
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DatabaseName);  // this is the path to your StreamingAssets in android
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
            // then save to Application.persistentDataPath
            File.WriteAllBytes(filepath, loadDb.bytes);
#elif UNITY_IOS
             var loadDb = Application.dataPath + "/Raw/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);
#elif UNITY_WP8
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
            // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);

#elif UNITY_WINRT
    		var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
    		// then save to Application.persistentDataPath
    		File.Copy(loadDb, filepath);
		
#elif UNITY_STANDALONE_OSX
    		var loadDb = Application.dataPath + "/Resources/Data/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
    		// then save to Application.persistentDataPath
    		File.Copy(loadDb, filepath);
#else
            var loadDb = Application.dataPath + "/StreamingAssets/" + DatabaseName;  // this is the path to your StreamingAssets in iOS
                                                                                     // then save to Application.persistentDataPath
            File.Copy(loadDb, filepath);

#endif

           
        }

        var dbPath = filepath;
#endif
        _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create);

    }

    public void InitiateDatabase()
    {
        CreateTablePlayerIfNotExists();
        CreateTableCategoryIfNotExists();
        CreaeLevelIfNotExists();
        CreateTableWordIfNotExists();
        CreateTableQuestionIfNotExists();
        CreateItemUnlockedIfNotExists();
        CreatePlayerProgressIfNotExists();
        CreateWordUsedIfNotExists();
        CreateQuestionUsedIfNotExists();
        CreateTableVersionAndInsert();


    }

    private void CreateTablePlayerIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'Player'( 'Id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 'name'  varchar, 'schoolarYear'  integer, 'coins' integer, 'avatar' varchar)");
    }

    private void CreateTableCategoryIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'Category'( 'Id' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 'name'  varchar, 'scholarYear' integer)");
    }

    private void CreaeLevelIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'Level' ('Id' integer NOT NULL PRIMARY KEY AUTOINCREMENT, 'number' integer, 'categoryId' integer, FOREIGN KEY('categoryId') REFERENCES 'Category'('Id'))");
    }

    private void CreateTableWordIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'Word'( 'id' INTEGER PRIMARY KEY, 'categoryId' INTEGER, 'word' TEXT, 'palavra' TEXT, 'example' TEXT, FOREIGN KEY('categoryId') REFERENCES 'Category'('Id'))");
    }

    private void CreateTableQuestionIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'Word'('id' INTEGER, 'categoryId' INTEGER, 'word'  TEXT, 'palavra'   TEXT, 'example'   TEXT, PRIMARY KEY('id'), FOREIGN KEY('categoryId') REFERENCES 'Category'('Id'))");
    }

    private void CreateItemUnlockedIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'ItemUnlocked' ('Id' INTEGER PRIMARY KEY AUTOINCREMENT, 'nameObject' TEXT, 'nameImage' TEXT, 'userId' INTEGER, 'isBeingUsed' INTEGER, 'positionX' REAL, 'positionY' REAL)");
    }

    private void CreatePlayerProgressIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'PlayerProgress' ('Id' INTEGER PRIMARY KEY AUTOINCREMENT, 'playerId' INTEGER, 'levelId' INTEGER, 'score' INTEGER, FOREIGN KEY('playerId') REFERENCES 'Player'('Id'), FOREIGN KEY('levelId') REFERENCES 'Level' ('Id'))");

        try
        {
            _connection.Execute("ALTER TABLE 'PlayerProgress' RENAME COLUMN 'RoundId' TO 'levelId'");
            _connection.Execute("ALTER TABLE 'PlayerProgress' RENAME COLUMN 'PlayerId' TO 'playerId'");
            _connection.Execute("ALTER TABLE 'PlayerProgress' RENAME COLUMN 'Score' TO 'score'");
        }
        catch (Exception) { }
    }

    private void CreateWordUsedIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'WordUsed' ('Id' INTEGER PRIMARY KEY AUTOINCREMENT, 'userId' INTEGER, 'wordId' INTEGER, 'categoryId' INTEGER, FOREIGN KEY('userId') REFERENCES 'Player' ('Id'), FOREIGN KEY('categoryId') REFERENCES 'Category' ('Id'), FOREIGN KEY('wordId') REFERENCES 'Word' ('id'))");
    }

    private void CreateQuestionUsedIfNotExists()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'QuestionUsed' ('Id' INTEGER PRIMARY KEY AUTOINCREMENT, 'userId' INTEGER, 'questionId' INTEGER, 'categoryId' INTEGER, FOREIGN KEY('userId') REFERENCES 'Player' ('Id'), FOREIGN KEY('categoryId') REFERENCES 'Category' ('Id'), FOREIGN KEY('questionId') REFERENCES 'Question' ('id'))");
    }

    private void CreateTableVersionAndInsert()
    {
        _connection.Execute("CREATE TABLE IF NOT EXISTS 'version' ('number' INTEGER, 'type' TEXT UNIQUE)");
        _connection.Execute("INSERT OR IGNORE INTO version(number, type) VALUES (0, 'word')");
        _connection.Execute("INSERT OR IGNORE INTO version (number, type) VALUES (0, 'question')");
    }

    public int VerifyWordVersion()
    {
        try
        {
            return _connection.ExecuteScalar<int>("SELECT number FROM version WHERE type = 'word'");
        } catch (SQLiteException e)
        {
            CreateTableVersionAndInsert();
            return 0;
        }
    }

    public int VerifyQuestionVersion()
    {
        try
        {
            return _connection.ExecuteScalar<int>("SELECT number FROM version WHERE type = 'question'");
        }
        catch (SQLiteException e)
        {
            CreateTableVersionAndInsert();
            return 0;
        }
    }

    public void UpdateWordVersion(int version)
    {
        _connection.Execute("UPDATE version SET number = ? WHERE type = 'word'", version);
    }

    public void UpdateQuestionVersion(int version)
    {
        _connection.Execute("UPDATE version SET number = ? WHERE type = 'question'", version);
    }

    public void DeleteTableWord()
    {
        _connection.Execute("DELETE FROM word");
    }

    public void InsertWord(Word w)
    {
        _connection.Insert(w);
    }

    public void DeleteTableQuestion()
    {
        _connection.Execute("DELETE FROM question");
    }

    public void InsertQuestion(Question q)
    {
        _connection.Insert(q);
    }

    public PlayerProgress getPlayerProgress(int playerId, int levelId)
    {
        return _connection.Table<PlayerProgress>().Where(x => x.playerId == playerId).Where(XboxBuildSubtarget => XboxBuildSubtarget.levelId == levelId).OrderBy(x => x.score).FirstOrDefault();
    }

    public Level GetLevelOfCategoryAndLevel(int categoryId, int level)
    {
        return _connection.Table<Level>().Where(x => x.categoryId == categoryId).Where(x => x.number == level).FirstOrDefault();
    }

    public Player GetPlayerById(int id)
    {
        return _connection.Table<Player>().Where(x => x.Id == id).FirstOrDefault();
    }

    public IEnumerable<Category> GetCategoriesByScholarYear(int scholarYear)
    {
        return _connection.Table<Category>().Where(x => x.scholarYear == scholarYear);
    }

    public IEnumerable<Word> GetWordsOfCategory(int id)
    {
        return _connection.Table<Word>().Where(x => x.categoryId == id);
    }

    public IEnumerable<Word> GetWordsOfgategoryOrderByWord(int id)
    {
        return _connection.Table<Word>().Where(x => x.categoryId == id).OrderBy(x => x.word);
    }

    public IEnumerable<Question> GetQuestionOfCategory(int id, int type)
    {
        return _connection.Table<Question>().Where(x => x.categoryId == id).Where(x => x.type == type);
    }

    public IEnumerable<Level> GetRoundsByCategory(int categoryId)
    {
        return _connection.Table<Level>().Where(x => x.categoryId == categoryId);
    }

    public void UpdatePlayer(Player currentPlayer)
    {
        _connection.Update(currentPlayer);
    }

    public void UpdateLevel(Level level)
    {
        _connection.Update(level);
    }

    public int InsertPlayer(Player newPlayer)
    {
        int registersInserted;
        if (GetPlayerByName(newPlayer.name) == null)
        {
            registersInserted = _connection.Insert(newPlayer);
            if (registersInserted == null)
            {
                throw new InsertException("ERRO: Jogador não inserido");
            }
        }
        else
        {
            throw new InsertException("ERRO: Nome já em uso");
        }

        return registersInserted;
    }

    public IEnumerable getAllPlayers()
    {
        return _connection.Table<Player>();
    }

    public void UpdatePlayerName(Player currentPlayer)
    {
        int registersInserted = _connection.Update(currentPlayer);
        if (registersInserted != 1)
        {
            throw new InsertException("ERRO: Jogador não atualizado");
        }

    }

    public IEnumerable<WordUsed> GetWordUsedOfCategory(int categoryId, int userId)
    {
        return _connection.Table<WordUsed>().Where(x => x.categoryId == categoryId).Where(x => x.userId == userId);
    }

    public void InsertWordUsed(Word word, Player player)
    {
        try
        {
            _connection.Insert(new WordUsed(player, word));
        }
        catch (SQLiteException)
        {
            _connection.CreateTable<WordUsed>();
            _connection.Insert(new WordUsed(player, word));
        }
    }

    public void DeleteUsedWordsOfCategory(int categoryId, int userId)
    {
        _connection.Execute("DELETE FROM WordUsed WHERE categoryId = ? AND userId = ?", categoryId, userId);
    }

    public IEnumerable<QuestionUsed> GetQuestionsUsedOfCategory(int categoryId, int userId)
    {
        return _connection.Table<QuestionUsed>().Where(x => x.categoryId == categoryId).Where(x => x.userId == userId);
    }

    public void InserQuestionUsed(Question question, Player player)
    {
        try
        {
            _connection.Insert(new QuestionUsed(player, question));
        }
        catch (SQLiteException)
        {
            _connection.CreateTable<QuestionUsed>();
            _connection.Insert(new QuestionUsed(player, question));
        }
    }

    public void DeleteQuestionUsedOfCategory(int categoryId, int playerId)
    {
        _connection.Execute("DELETE FROM QuestionUsed WHERE categoryId = ? AND userId = ?", categoryId, playerId);
    }

    public void UpdatePlayerAvatar(Player currentPlayer)
    {
        int registersInserted = _connection.Update(currentPlayer);
        if (registersInserted != 1)
        {
            throw new InsertException("ERRO: Jogador não atualizado");
        }

    }

    public void UpdatePlayerYear(Player currentPlayer)
    {
        int registersInserted = _connection.Update(currentPlayer);
        if (registersInserted > 1)
        {
            throw new InsertException("ERRO: Jogador não atualizado");
        }

    }

    public Player GetPlayerByName(string name)
    {
        return _connection.Table<Player>().Where(x => x.name == name).FirstOrDefault();
    }

    public void SaveScore(int playerId, int roundId, int score)
    {
        _connection.Insert(new PlayerProgress(playerId, roundId, score));
    }

    public Level GetLevel(int number, int categoryId)
    {
        return _connection.Table<Level>().Where(x => x.number == number).Where(x => x.categoryId == categoryId).FirstOrDefault();
    }

    internal void DeletePlayer(int playerIdToDelete)
    {
        _connection.Delete(GetPlayerById(playerIdToDelete));
    }

    public IEnumerable<PlayerProgress> GetAllPlayerProgress()
    {
        return _connection.Table<PlayerProgress>();
    }

    public int GetLastLevelOfCategoryDoneByAPlayer(int playerId, int categoryId)
    {
        return _connection.ExecuteScalar<int>("SELECT MAX(Level.number) FROM PlayerProgress JOIN Level ON Level.Id = PlayerProgress.levelId WHERE PlayerId = ? AND Level.categoryId = ?", playerId, categoryId);
    }

    public void DeletePlayerProgressOfPlayer(int playerIdToDelete)
    {
        _connection.Execute("DELETE FROM PlayerProgress Where PlayerId = ?", playerIdToDelete);
    }

    public void DeleteItemUnlockedOfPlayer(int id)
    {
        _connection.Execute("DELETE FROM ItemUnlocked Where userId = ?", id);
    }

    public void DeleteWordUsedOfPlayer(int id)
    {
        _connection.Execute("DELETE FROM WordUsed Where userId = ?", id);
    }

    public void DeleteQuestionUsedOfPlayer(int id)
    {
        _connection.Execute("DELETE FROM QuestionUsed Where userId = ?", id);
    }

    public void InsertItemUnlocked(ItemUnlocked item)
    {
        int registersInserted = _connection.Insert(item);
        if (registersInserted != 1)
        {
            throw new InsertException("ERRO: Item não desbloqueado");
        }
    }

    public void UpdateItem(ItemUnlocked item)
    {
        int registersUpdated = _connection.Update(item);
        if (registersUpdated != 1)
        {
            throw new InsertException("ERRO: Item não atualizado");
        }
    }

    public ItemUnlocked GetItemByName(string nameObject, int userID)
    {
        return _connection.Table<ItemUnlocked>().Where(x => x.userId == userID && x.nameObject == nameObject).FirstOrDefault();
    }

    public ItemUnlocked GetItemByImageName(int userID, string nameImagem)
    {
        return _connection.Table<ItemUnlocked>().Where(x => x.userId == userID && x.nameImage == nameImagem).FirstOrDefault();
    }

    public IEnumerable<ItemUnlocked> GetItemsByUserID(int userID)
    {
        return _connection.Table<ItemUnlocked>().Where(x => x.userId == userID);
    }

    public IEnumerable<ItemUnlocked> GetItemsBeingUsedByUserID(int userID)
    {
        return _connection.Table<ItemUnlocked>().Where(x => x.userId == userID && x.isBeingUsed == true);
    }

    /* SQL Exceptions */

    public class InsertException : Exception
    {
        public InsertException(string message) : base(message) { }

    }


}
