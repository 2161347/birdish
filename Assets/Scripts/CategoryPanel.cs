﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CategoryPanel : MonoBehaviour
{
    [SerializeField] 
    private Text categoryName;

    [SerializeField]
    private Image buttonImage;

    private GameObject levelPanel;
    private Text levelText;
    private GameObject gamePanel;
    private MenuController mc;

    private Category category;
    private Persistence persistence = Persistence.Instance;
    private int level;

    public void Setup(Category category, MenuController mc)
    {
        this.category = category;
        this.mc = mc;
        level = persistence.GetLastLevelOfCategoryDoneByMe(category.Id);
        SetupButton();
    }

    private void SetupButton()
    {
        if(category != null) {

            categoryName.text = category.name;

            buttonImage.sprite = Resources.Load<Sprite>("category/" + category.scholarYear + "Y/Level" + (level+1) + "/" + category.name);
        }
    }

    public void ButtonOnClick()
    {
        if(level < 4)
        {
            level++;
        }

        AudioSource audio = mc.GetAudioSource();
        audio.PlayOneShot((AudioClip)Resources.Load("Audio/Play"));

        mc.SetGamePanel(category, level);
    }
}
