﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class DragAndDropGame : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Button btn1;
    [SerializeField] private Text textBtn1;
    [SerializeField] private Button btn2;
    [SerializeField] private Text textBtn2;
    [SerializeField] private Button btn3;
    [SerializeField] private Text textBtn3;
    private MenuController menuController;


    public void Setup(List<Word> words, MenuController menuController)
    {

        this.menuController = menuController;
        System.Random random = new System.Random();

        image.sprite = Resources.Load<Sprite>("GameImages/" + words[0].categoryId + "/" + words[0].word.ToLower());
        image.preserveAspect = true;

        if (words[0].categoryId == 1)
        {
            Dictionary<string, List<string>> pairs = Persistence.Instance.Pairs;
            List<string> possibleWords = pairs[words[0].word];
            int x = random.Next(0, possibleWords.Count);
            int y;
            do
            {
                y = random.Next(0, possibleWords.Count);
            } while (y == x);

            words[1].word = possibleWords[x];
            words[2].word = possibleWords[y];

        }

        int i = random.Next(1, 4);

        switch (i)
        {
            case 1:
                SetButton1(words[0].word, true);
                SetButton2(words[1].word, false);
                SetButton3(words[2].word, false);
                break;
            case 2:
                SetButton1(words[1].word, false);
                SetButton2(words[0].word, true);
                SetButton3(words[2].word, false);
                break;
            case 3:
                SetButton1(words[2].word, false);
                SetButton2(words[1].word, false);
                SetButton3(words[0].word, true);
                break;
            default:

                break;
        }
    }

    private void SetButton1(string text, bool action)
    {
        textBtn1.text = text;
        if (action)
        {
            btn1.onClick.AddListener(menuController.DragAndDropCorrectBtnOnClick);
        } else
        {
            btn1.onClick.AddListener(menuController.DragAndDropIncorrectBtnOnClick);
        }
    }

    private void SetButton2(string text, bool action)
    {
        textBtn2.text = text;
        if (action)
        {
            btn2.onClick.AddListener(menuController.DragAndDropCorrectBtnOnClick);
        }
        else
        {
            btn2.onClick.AddListener(menuController.DragAndDropIncorrectBtnOnClick);
        }
    }

    private void SetButton3(string text, bool action)
    {
        textBtn3.text = text;
        if (action)
        {
            btn3.onClick.AddListener(menuController.DragAndDropCorrectBtnOnClick);
        }
        else
        {
            btn3.onClick.AddListener(menuController.DragAndDropIncorrectBtnOnClick);
        }
    }
}
