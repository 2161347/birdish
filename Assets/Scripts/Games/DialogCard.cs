﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogCard : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Button soundButton;
    [SerializeField] private Button nextButton;
    private MenuController menuController;
    private AudioClip clip;

    public void Setup(Word w, MenuController menuController) 
    {
        this.menuController = menuController;

        image.sprite = Resources.Load<Sprite>("GameImages/" + w.categoryId + "/" + w.word.ToLower());
        image.preserveAspect = true;

        clip = (AudioClip)Resources.Load("Audio/Games/" + w.categoryId + "/" + w.word.ToLower());
    }

    public void BtnSoundClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        audio.PlayOneShot(clip);
    }

    public void BtnOnClick()
    {
        menuController.DialogCardButtonOnClick();
    }
}
