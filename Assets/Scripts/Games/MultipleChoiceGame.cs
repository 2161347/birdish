﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class MultipleChoiceGame : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private Button btn1;
    [SerializeField] private Text textBtn1;
    [SerializeField] private Button btn2;
    [SerializeField] private Text textBtn2;
    private MenuController menuController;
    private int level;

    public void Setup(int level, Question question, MenuController controller)
    {

        text.text = question.question;
        menuController = controller;
        this.level = level;

        SetButtons(question.correct, question.wrong);
    }

    private void SetButtons(string correctOption, string incorrectOption)
    {
        System.Random random = new System.Random();

        int i = random.Next(0, 2);

        if (i == 0)
        {
            textBtn1.text = correctOption;
            textBtn2.text = incorrectOption;
            btn1.onClick.AddListener(CorrectClick);
            btn2.onClick.AddListener(IncorrectClick);
        }
        else
        {
            textBtn2.text = correctOption;
            textBtn1.text = incorrectOption;
            btn2.onClick.AddListener(CorrectClick);
            btn1.onClick.AddListener(IncorrectClick);
        }
    }

    private void CorrectClick()
    {
        if(level == 3)
        {
            menuController.MultipleChoiceCorrectClickLevel3();
        } else
        {
            menuController.MultipleChoiceCorrectClickLevel4();
        }

    }

    private void IncorrectClick()
    {
        if(level == 3)
        {
            menuController.MultipleChoiceInCorrectClickLevel3();
        } else
        {
            menuController.MultipleChoiceInCorrectClickLevel4();
        }

    }
}
