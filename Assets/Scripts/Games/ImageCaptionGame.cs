﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Linq;

public class ImageCaptionGame : MonoBehaviour
{

    [SerializeField] private Image image;
    [SerializeField] private InputField inputField;
    [SerializeField] private GameObject soundButton;
    private Word word;
    private MenuController menuController;

    public void Setup(MenuController menuController, Word v)
    {
        this.menuController = menuController;
        word = v;
        image.sprite = Resources.Load<Sprite>("GameImages/" + v.categoryId + "/" + v.word);
        image.preserveAspect = true;

        inputField.ActivateInputField();
        inputField.Select();
    }

    public void SubmitOnClick()
    {
        string correct = word.word.Trim().ToUpper();
        string incorrect = inputField.text.Trim().ToUpper();
        string strWithoutSpecial = new string(correct.Where(x => char.IsWhiteSpace(x) || char.IsLetterOrDigit(x)).ToArray());

        if (incorrect.Equals(word.word.ToUpper()))
        {
            menuController.ImageCaptionCorrect();
        } else
        {
            soundButton.SetActive(true);

            if (string.Equals(strWithoutSpecial, incorrect.Replace(" ", "")) && (word.word.Contains('-') || word.word.Contains("'")) && incorrect != "")
            {
                menuController.ShowMessage(true, "A special character is missing. Ex.: ' or  - .");
            }
        }
    }

    public void SoundButtonOnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip)Resources.Load("Audio/Games/" + word.categoryId + "/" + word.word.ToLower());
        audio.PlayOneShot(clip);
    }
}
