﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class FillInTheBlanksGame : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] GameObject letterPanel;
    [SerializeField] GameObject letterPrefab;
    [SerializeField] GameObject letterInputPrefab;
    [SerializeField] GameObject soundButton;
    private MenuController menuController;
    private string word;
    private Word w;
    private char[] replacedWord;

    private int lettersPlaced = 0;
    private int max;

    public void Setup(MenuController menuController, Word v)
    {
        this.menuController = menuController;
        this.word = v.word.ToUpper();
        this.w = v;
        this.replacedWord = word.ToCharArray();
        image.sprite = Resources.Load<Sprite>("GameImages/" + v.categoryId + "/" + v.word.ToLower());
        image.preserveAspect = true;
        SetGame();
    }

    private void SetGame()
    {
        ChooseLettersToRemove();
        for(int i = 0; i < replacedWord.Length; i++)
        {
            GameObject g;

            if (char.IsWhiteSpace(word[i]))
                continue;

            if (replacedWord[i] == char.Parse("?"))
            {
                g = Instantiate(letterInputPrefab);
                g.GetComponent<LetterInputFITB>().SetFillInTheBlanksGame(this, word[i].ToString());
            }else
            {
                g = Instantiate(letterPrefab);
                g.GetComponent<LetterPlaceFITB>().SetText(replacedWord[i]);
            }

            g.transform.SetParent(letterPanel.transform, false);
            g.transform.localPosition = new Vector2((i - word.Length / 2) * 80, g.transform.localPosition.y);
        }


    }

    private void ChooseLettersToRemove()
    {
        List<int> indexRemoved = new List<int>();
        max = ((word.Length - 2) % 2) + ((word.Length - 2) / 2);
        System.Random random = new System.Random();
        int i = 0;

        do
        {
            int x = random.Next(1, word.Length - 1);

            if (!char.IsLetter(word[x]) || indexRemoved.Contains(x))
            {
                continue;
            }
            else
            {
                replacedWord[x] = char.Parse("?");
                indexRemoved.Add(x);
            }

        } while (indexRemoved.Count < max);
    }

    private int WordSpaces()
    {
        int i = 0;
        foreach (char c in word)
        {
            if (char.IsWhiteSpace(c))
            {
                i++;
            }
        }
        return i;
    }

    public void LetterPlaced()
    {
        lettersPlaced++;

    }

    public void NextButtonOnClick()
    {
        if (lettersPlaced >= max)
        {
            menuController.FillInTheBlanksTerminated();
        }
    }

    public void ActivateSoundButton()
    {
        soundButton.SetActive(true);
    }

    public void SoundButtonOnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip)Resources.Load("Audio/Games/" + w.categoryId + "/" + w.word.ToLower());
        audio.PlayOneShot(clip);
    }
}
