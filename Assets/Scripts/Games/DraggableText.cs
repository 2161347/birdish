﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DraggableText : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField] private Text text;
    private string word;
    private WordOrderGame wordOrderGame;
    private Vector2 initialPosition;
    private InputField input;

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (input.GetComponent<BoxCollider2D>().OverlapPoint(transform.position))
        {
            input.text += word + " ";
            Destroy(this.gameObject);
            wordOrderGame.VerifyIfGameEnded();
        }
    }

    public void Setup(WordOrderGame wordOrderGame, string word, InputField input)
    {
        this.word = word;
        text.text = word;
        this.wordOrderGame = wordOrderGame;
        this.input = input;
        this.initialPosition = transform.localPosition;
    }
}
