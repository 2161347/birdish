﻿using UnityEngine;
using UnityEngine.UI;

public class LetterInputFITB : MonoBehaviour
{
    [SerializeField] private InputField inputField;
    private string letter;
    private FillInTheBlanksGame fillInTheBlanksGame;

    public void EndInputText()
    {
        if (inputField.text.Equals(letter))
        {
            inputField.readOnly = true;
            fillInTheBlanksGame.LetterPlaced();
        } else
        {
            inputField.text = "";
            fillInTheBlanksGame.ActivateSoundButton();
        }
    }

    public void SetFillInTheBlanksGame(FillInTheBlanksGame fillInTheBlanksGame, string s)
    {
        this.fillInTheBlanksGame = fillInTheBlanksGame;
        this.letter = s;
    }
}

