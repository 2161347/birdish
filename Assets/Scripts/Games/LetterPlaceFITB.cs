﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LetterPlaceFITB : MonoBehaviour
{
    [SerializeField] private Text text;

    public void SetText(char c)
    {
        text.text = c.ToString();
    }
}
