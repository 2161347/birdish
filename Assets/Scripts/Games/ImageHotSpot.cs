﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageHotSpot : MonoBehaviour
{
    [SerializeField] private Button btn1;
    [SerializeField] private Button btn2;
    private List<Word> words;
    private MenuController menuController;

    public void Setup(List<Word> words, MenuController menuController)
    {
        this.menuController = menuController;
        this.words = words;
        btn1.image.sprite = Resources.Load<Sprite>("GameImages/" + words[0].categoryId + "/" + words[0].word.ToLower());
        btn1.image.preserveAspect = true;
        btn2.image.sprite = Resources.Load<Sprite>("GameImages/" + words[1].categoryId + "/" + words[1].word.ToLower());
        btn2.image.preserveAspect = true;
    }

    public void Btn1OnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip)Resources.Load("Audio/Games/" + words[0].categoryId + "/" + words[0].word.ToLower());
        audio.PlayOneShot(clip);
    }

    public void Btn2OnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip)Resources.Load("Audio/Games/" + words[1].categoryId + "/" + words[1].word.ToLower());
        audio.PlayOneShot(clip);
    }

    public void NextButtonOnClick()
    {
        menuController.ImageHotSpotButtonOnClick();
    }
}
