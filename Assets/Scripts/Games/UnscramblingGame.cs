﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text;

public class UnscramblingGame : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private GameObject wordPanel;
    [SerializeField] private GameObject letterPanel;
    [SerializeField] private GameObject letterPrefab;
    [SerializeField] private GameObject letterPlacePrefab;
    private string word;
    private string trimmedWord;
    private MenuController menuController;
    private List<GameObject> letterPlaces;
    private int lettersPlaced = 0;
    private int specialChars = 0;

    public void Setup(MenuController controller, Word s)
    {
        image.sprite = Resources.Load<Sprite>("GameImages/" + s.categoryId + "/" + s.word.ToLower());
        image.preserveAspect = true;
        menuController = controller;
        word = s.word.ToUpper();
        trimmedWord = s.word.Replace(" ", "").ToUpper();
        letterPlaces = new List<GameObject>();

        SetWordPlaces();
        SetLettersPlaces();
    }

    private void SetWordPlaces()
    {
        int whiteSpace = 0;

        for(int i = 0; i < word.Length; i++)
        {
            if (!char.IsWhiteSpace(word[i]))
            {
                GameObject g = Instantiate(letterPlacePrefab);
                g.transform.SetParent(wordPanel.transform, false);
                g.transform.localPosition = new Vector2(((i - word.Length/2) * 74 - (whiteSpace * 30)), g.transform.localPosition.y);
                g.GetComponent<LetterPlace>().Letter = word[i];
                letterPlaces.Add(g);
            } else
            {
                whiteSpace++;
            }
        }
    }

    private void SetLettersPlaces()
    {
        string shuffled = Shuffle(trimmedWord);

        for (int i = 0; i < shuffled.Length; i++)
        {
            if (char.IsLetter(shuffled[i]))
            {
                GameObject g = Instantiate(letterPrefab);
                g.transform.SetParent(letterPanel.transform, false);
                g.transform.localPosition = new Vector2(((i - word.Length/2 - specialChars) * 80), g.transform.localPosition.y);
                g.GetComponent<DragLetter>().Setup(shuffled[i], PossiblePlacesOfChar(shuffled[i]), this);
            } else
            {
                List<GameObject> places = PossiblePlacesOfChar(shuffled[i]);
                GameObject g = Instantiate(letterPrefab);
                g.transform.SetParent(places[0].transform, false);
                g.GetComponent<DragLetter>().Setup(shuffled[i], places, this);
                specialChars++;
            }
        }
    }

    private List<GameObject> PossiblePlacesOfChar(char c)
    {
        List<GameObject> possiblePlaces = new List<GameObject>();

        foreach(GameObject g in letterPlaces)
        {
            if (g.GetComponent<LetterPlace>().Letter.Equals(c))
            {
                possiblePlaces.Add(g);
            }
        }

        return possiblePlaces;
    }

    private string Shuffle(string s)
    {
        System.Random random = new System.Random();
        int index;

        List<char> chars = new List<char>(s);
        StringBuilder sb = new StringBuilder();
        while (chars.Count > 0)
        {
            index = random.Next(chars.Count);
            sb.Append(chars[index]);
            chars.RemoveAt(index);
        }
        return sb.ToString();
    }

    public void LetterPlaced()
    {
        lettersPlaced++;

        if(lettersPlaced >= (trimmedWord.Length - specialChars))
        {
            menuController.UnscramblingTerminated();
        }
    }
}
