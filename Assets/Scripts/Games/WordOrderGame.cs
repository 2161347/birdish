﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class WordOrderGame : MonoBehaviour
{
    [SerializeField] private InputField inputField;
    [SerializeField] private GameObject wordPanel;
    [SerializeField] private GameObject draggableText;
    private MenuController menuController;
    private Question question;
    private string response;
    private string[] responseSplited;
    private List<GameObject> objects;

    public void Setup(MenuController menuController, Question question)
    {
        this.question = question;
        this.menuController = menuController;
        responseSplited = question.question.Split(' ');

        for(int i = 0; i < responseSplited.Length; i++)
        {
            if (responseSplited[i].Contains("_"))
            {
                responseSplited[i] = question.correct;
            }

            response += responseSplited[i] + " ";
        }

        response = response.Trim();

        responseSplited = Shuffle(responseSplited);

        InstantiateDraggableText();
    }

    public void RepeatButtonOnClick()
    {
        DestroyDraggableText();
        inputField.text = "";
        InstantiateDraggableText();
    }

    public void VerifyIfGameEnded()
    {
        string s = inputField.text.Trim();
        if (response.Length == inputField.text.Trim().Length)
        {
            if (response.Equals(s))
            {
                menuController.WordOrderTerminated(response.Length, s.Length);
            } else
            {
                menuController.WordOrderTerminated(response.Length, 0);
            }
        }
    }

    private int CalculateNumberOfCorrectsPlacedWords()
    {
        int w = 0;
        string[] ss = inputField.text.Trim().Split(' ');
        for(int i = 0; i < ss.Length; ++i)
        {
            if(ss[i] == responseSplited[i])
            {
                w++;
            }
        }

        return w;
    }

    private string[] Shuffle(string[] s)
    {
        System.Random random = new System.Random();
        int index;

        List<string> frase = responseSplited.OfType<string>().ToList();

        List<string> sb = new List<string>();
        while (frase.Count > 0)
        {
            index = random.Next(frase.Count);
            sb.Add(frase[index]);
            frase.RemoveAt(index);
        }
        return sb.ToArray();
    }

    private void InstantiateDraggableText()
    {
        int x = 0;
        objects = new List<GameObject>();
        foreach (string w in responseSplited)
        {
            GameObject g = Instantiate(draggableText);
            g.transform.SetParent(wordPanel.transform, false);
            g.GetComponent<DraggableText>().Setup(this, w, inputField);
            objects.Add(g);
            x++;
        }
    }

    private void DestroyDraggableText()
    {
        foreach(GameObject g in objects)
        {
            Destroy(g);
        }
        objects.Clear();
    }
}
