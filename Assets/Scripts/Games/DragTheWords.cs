﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;

public class DragTheWords : MonoBehaviour
{
    [SerializeField] private Image image1;
    [SerializeField] private GameObject boxImg1;
    [SerializeField] private Image image2;
    [SerializeField] private GameObject boxImg2;
    [SerializeField] private Text[] texts;
    private int numberOfCorrectPlaced = 0;
    private MenuController menuController;
    private Dictionary<string, List<string>> pairs = Persistence.Instance.Pairs;

    public void Setup(List<Word> words, MenuController controller)
    {
        menuController = controller;
        System.Random random = new System.Random();

        image1.sprite = Resources.Load<Sprite>("GameImages/" + words[0].categoryId + "/" + words[0].word.ToUpper());
        image1.preserveAspect = true;


        if (words[0].categoryId == 1)
        {

            SetGame(words);
            return;
        }

        image2.sprite = Resources.Load<Sprite>("GameImages/" + words[1].categoryId + "/" +  words[1].word.ToUpper());
        image2.preserveAspect = true;


        int i = 0;

        do
        {
            int r = random.Next(0, this.texts.Length);

            if(this.texts[r].text.Equals("New Text"))
            {

                
                this.texts[r].text = words[i].word;

                if(i == 0)
                {

                    this.texts[r].GetComponent<DragText>().Setup(boxImg1, CorrectPositionOfWord);
                }
                    

                if (i == 1)
                {
                    this.texts[r].GetComponent<DragText>().Setup(boxImg2, CorrectPositionOfWord);
                }
                    

                i++;
            } else
            {
                continue;
            }

        } while (i < this.texts.Length);
    }

    private void SetGame(List<Word> words)
    {
        List<string> possibleWords = pairs[words[0].word];

        int x = 1;

        do
        {
            x++;
        } while (!possibleWords.Contains(words[x].word));

        words[1].word = possibleWords[x];

        List<string> possibleWords2 = pairs[words[1].word];
        List<string> resultList = possibleWords.Intersect(possibleWords2).ToList();

        image2.sprite = Resources.Load<Sprite>("GameImages/" + words[1].categoryId + "/" + words[1].word.ToUpper());
        image2.preserveAspect = true;

        int i = 0;

        System.Random random = new System.Random();

        do
        {
            int r = random.Next(0, texts.Length);

            if (this.texts[r].text.Equals("New Text"))
            {
                if (i == 0)
                {
                    this.texts[r].text = words[0].word;
                    this.texts[r].GetComponent<DragText>().Setup(boxImg1, CorrectPositionOfWord);
                } else if (i == 1)
                {
                    this.texts[r].text = words[1].word;
                    this.texts[r].GetComponent<DragText>().Setup(boxImg2, CorrectPositionOfWord);
                } else
                {
                    this.texts[r].text = resultList[r];
                }

                i++;
            }
            else
            {
                continue;
            }

        } while (i < this.texts.Length);
    }

    private void CorrectPositionOfWord()
    {
        numberOfCorrectPlaced++;

        if(numberOfCorrectPlaced >= 2)
        {
            menuController.DragTheWordsTerminated();
        }
    }
}
