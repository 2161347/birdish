﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageHotspotGame : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private Button[] buttons;
    private MenuController menuController;

    public void Setup(List<Word> words, MenuController menuController)
    {
        this.text.text = words[0].word;

        System.Random random = new System.Random();
        int r = random.Next(0, words.Count);
        int x = 0;

        for(int i = 0; i < buttons.Length; i++)
        {
            if(i == r)
            {
                buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("GameImages/" + words[0].categoryId + "/" + words[0].word.ToUpper());
                buttons[i].onClick.AddListener(menuController.ImageHotSpotCorrectClick);
            } else
            {
                buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("GameImages/" + words[x+1].categoryId + "/" + words[x+1].word);
                buttons[i].onClick.AddListener(menuController.ImageHotSpotIncorrectClick);
                x++;
            }
        }
    }



}
