﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TrueOrFalseGame : MonoBehaviour
{
    [SerializeField] private Text text;
    private bool response;
    private MenuController menuController;


    public void Setup(Question question, MenuController menuController)
    {
        System.Random random = new System.Random();
        this.response = random.NextDouble() >= 0.5;
        text.text = SetQuestion(question, response);
        this.menuController = menuController;
    }

    private string SetQuestion(Question question, bool response)
    {
        string result = "";
        var s = question.question.Split(' ');

        foreach(string a in s)
        {
            if (a.Contains("_"))
            {
                if (response)
                {
                    result += question.correct;
                } else
                {
                    result += question.wrong;
                }
            } else
            {
                result += a;
            }

            result += " ";
        }

        return result.Trim();
        
    }

    public void TrueButtonOnClick()
    {
        if (response)
        {
            menuController.TrueFalseCorrectOnClick();
        } else
        {
            menuController.TrueFalseIncorrectOnClick();
        }
    }

    public void FalseButtonClick()
    {
        if (response)
        {
            menuController.TrueFalseIncorrectOnClick();
        }
        else
        {
            menuController.TrueFalseCorrectOnClick();
        }
    }
}
