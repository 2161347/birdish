﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Linq;

public class FillinTheBlanksWordsGame : MonoBehaviour
{
    [SerializeField] private Text answer;
    [SerializeField] private InputField response;
    [SerializeField] private Button soundButton;
    [SerializeField] private Image image;
    private MenuController menuController;
    private string word;
    private Question question;

    public void Setup(MenuController menuController, Question question)
    {
        this.menuController = menuController;
        this.question = question;
        answer.text = question.question;
        word = question.correct;
        image.sprite = Resources.Load<Sprite>("GameImages/FillInTheBlanks/" + question.categoryId + "/" + question.id);
        image.preserveAspect = true;

        response.ActivateInputField();
        response.Select();
    }

    public void BtnOnClick()
    {
        string correct = word.Trim().ToUpper();
        string incorrect = response.text.Trim().ToUpper();
        string strWithoutSpecial = new string(correct.Where(x => char.IsWhiteSpace(x) || char.IsLetterOrDigit(x)).ToArray());



        if (correct.Equals(incorrect))
        {
            menuController.FillInTheBlanksWordCorrect();
        } else
        {
            soundButton.gameObject.SetActive(true);

            if (string.Equals(strWithoutSpecial, incorrect.Replace(" ", "")) && (word.Contains('-') || word.Contains("'")) && incorrect != "")
            {
                menuController.ShowMessage(true, "A special character is missing. Ex.: ' or  - .");
            }
        }
    }

    public void SoundBtnOnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip) Resources.Load("Audio/FillInTheBlanks/" + question.categoryId + "/" + question.id);
        audio.PlayOneShot(clip);
        response.ActivateInputField();
        response.Select();
    }
}
