﻿using UnityEngine;
using System.Collections;

public class LetterPlace : MonoBehaviour
{
    private char letter;
    private bool ocupied = false;

    public char Letter
    {
        get { return letter; }
        set { letter = value; }
    }

    public bool Ocupied
    {
        get { return ocupied; }
        set { ocupied = value; }
    }
}
