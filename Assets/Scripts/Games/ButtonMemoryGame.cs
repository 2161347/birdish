﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ButtonMemoryGame : MonoBehaviour
{
    public static bool STOP = false;

    [SerializeField] private Text text;
    [SerializeField] private Image innerImage;
    private MemoryGame memoryGame;
    private bool inGame = true;
    private int buttonIndex;
    private string word;

    public void Setup(MemoryGame game, int i, string s)
    {
        buttonIndex = i;
        memoryGame = game;
        word = s;
        SetText(s);
    }

    public void FlippCardFace()
    {
        if (!STOP && inGame)
        {
            this.GetComponent<Image>().sprite = Resources.Load<Sprite>("mg_other");
            text.gameObject.SetActive(true);
            memoryGame.MemoryButtonClick(buttonIndex);
        }
    }

    public void FlipCardBack()
    {
        this.GetComponent<Image>().sprite = Resources.Load<Sprite>("mg_face");
        text.gameObject.SetActive(false);
    }

    public string Word
    {
        get { return word; }

        set { word = value; }
    }

    private void SetText(string s)
    {
        text.text = s.Replace('-', '\n');
    }
}
