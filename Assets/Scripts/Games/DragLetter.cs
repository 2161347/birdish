﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class DragLetter : MonoBehaviour, IDragHandler, IEndDragHandler
{

    private const int minDistance = 45;



    [SerializeField] private Text text;

    private UnscramblingGame unscramblingGame;
    private List<GameObject> places;
    private Vector2 initialPosition;
    private bool placed = false;

    public void Setup(char c, List<GameObject> list, UnscramblingGame unscramblingGame)
    {
        text.text = c.ToString();
        places = list;
        this.unscramblingGame = unscramblingGame;
        initialPosition = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(!placed)
            transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!placed)
        {
            GameObject g = OnPositionWithSomePlace(transform.position);

            if (g != null)
            {
                transform.position = g.transform.position;
                placed = true;
                unscramblingGame.LetterPlaced();
            }
            else
            {
                transform.position = initialPosition;
            }
        }
    }

    private GameObject OnPositionWithSomePlace(Vector3 actualPosition)
    {

        foreach(GameObject g in places)
        {
            LetterPlace letterPlace = g.GetComponent<LetterPlace>();
            if (Vector3.Distance(actualPosition, g.transform.position) <= minDistance && !letterPlace.Ocupied)
            {
                letterPlace.Ocupied = true;
                return g;
            }
        }

        return null;
    }
}
