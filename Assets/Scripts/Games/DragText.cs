﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragText : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    private const int minDistance = 50;
    private Vector3 initialPosition;
    private Action correctAction;
    private GameObject destinationBox;

    public void Setup(GameObject gameObject, Action action)
    {
        this.correctAction = action;
        destinationBox = gameObject;
    }

    public void Setup()
    {
        Setup(null, null);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if(destinationBox == null || transform.position != destinationBox.transform.position)
            transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if(destinationBox != null)
        {
            if (Vector3.Distance(destinationBox.transform.position, transform.position) <= minDistance && transform.position != destinationBox.transform.position)
            {
                transform.position = destinationBox.transform.position;
                correctAction();
                return;
            }
        } 

        transform.position = initialPosition;

    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        initialPosition = transform.position;
    }
}
