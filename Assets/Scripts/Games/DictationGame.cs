﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Linq;

public class DictationGame : MonoBehaviour
{
    [SerializeField] private Button dictationButton;
    [SerializeField] private InputField inputField;
    private string word;
    private MenuController menuController;
    private int level;
    private AudioClip clip;

    public void Setup(int level, MenuController menuController, Word v)
    {
        this.word = v.word.ToUpper();
        this.menuController = menuController;
        this.level = level;

        clip = (AudioClip)Resources.Load("Audio/Games/" + v.categoryId + "/" + v.word.ToLower());
    }

    public void OnEndInput()
    {
        string correct = word.Trim().ToUpper();
        string incorrect = inputField.text.Trim().ToUpper();
        string strWithoutSpecial = new string(correct.Where(x => char.IsWhiteSpace(x) || char.IsLetterOrDigit(x)).ToArray());

        if (correct.Equals(incorrect))
        {
            if(level == 3)
            {
                menuController.DictationGameTerminatedLevel3();
            } else
            {
                menuController.DictationGameTerminatedLevel4();
            }
        } else if (string.Equals(strWithoutSpecial, incorrect.Replace(" ", "")) && (word.Contains('-') || word.Contains("'")) && incorrect != "")
        {
            menuController.ShowMessage(true, "A special character is missing. Ex.: ' or  - .");
        }
    }

    public void dictationButtonOnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        audio.PlayOneShot(clip);

        inputField.ActivateInputField();
        inputField.Select();
    }
}
