﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemoryGame : MonoBehaviour
{
    [SerializeField] Button[] buttons;
    private List<Button> buttonClicked = new List<Button>();
    private int matches = 0;
    private MenuController menuController;

    public void Setup(MenuController controller, List<Word> words)
    {
        menuController = controller;
        InitializeCard(words);
    }

    private void InitializeCard(List<Word> words)
    {

        System.Random random = new System.Random();

        for(int i = 0; i < words.Count; i++)
        {
            for(int y = 0; y < 2; y++)
            {
                int x = 0;

                do
                {
                    x = random.Next(0, buttons.Length);
                } while (buttons[x].GetComponent<ButtonMemoryGame>().Word != null);

                buttons[x].GetComponent<ButtonMemoryGame>().Setup(this, x, words[i].word);
            }
        }
    }

    public void MemoryButtonClick(int i)
    {
        if(buttonClicked.Count < 1)
        {
            buttonClicked.Add(buttons[i]);
        } else
        {
            buttonClicked.Add(buttons[i]);
            ButtonMemoryGame.STOP = true;

            StartCoroutine(CheckButtons());
        }
    }

    private IEnumerator CheckButtons()
    {

        yield return new WaitForSeconds(0.75f);

        ButtonMemoryGame buttonMemoryGame0 = buttonClicked[0].GetComponent<ButtonMemoryGame>();
        ButtonMemoryGame buttonMemoryGame1 = buttonClicked[1].GetComponent<ButtonMemoryGame>();

        if (buttonMemoryGame0.Word != buttonMemoryGame1.Word)
        {
            buttonMemoryGame0.FlipCardBack();
            buttonMemoryGame1.FlipCardBack();
            ButtonMemoryGame.STOP = false;
            buttonClicked.Clear();
        } else
        {
            matches++;
            ButtonMemoryGame.STOP = false;
            buttonClicked.Clear();

            if(matches >= 6)
            {
                menuController.MemoryGameTerminated();
            }
        }
    }
}
