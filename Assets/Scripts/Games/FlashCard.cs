﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlashCard : MonoBehaviour
{
    [SerializeField] private Text panelText;
    [SerializeField] private Button nextButton;

    public void Setup(IEnumerable words, Action nextBtnOnClick)
    {
        foreach(Word w in words)
        {
            panelText.text += w.word + "\n";
        }

        nextButton.onClick.AddListener(delegate { nextBtnOnClick(); });
    }
}
