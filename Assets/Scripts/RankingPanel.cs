﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingPanel : MonoBehaviour
{
    [SerializeField] Image avatar;
    [SerializeField] Text position;
    [SerializeField] Text name;
    [SerializeField] Text points;

    public void Setup(Player p, int position, int points)
    {
        avatar.sprite = Resources.Load<Sprite>("avatars/" + p.avatar);
        this.position.text = position.ToString();
        this.name.text = p.name;
        this.points.text = points.ToString();
    }
}
