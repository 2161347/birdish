﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnterController : MonoBehaviour
{
    [SerializeField] private GameObject secondPanel;
    private Persistence persistence;

    void Start()
    {
        persistence = Persistence.Instance;


        if (!PlayerPrefs.HasKey("somOn"))
        {
            PlayerPrefs.SetInt("somOn", 1);
        }

        if (!PlayerPrefs.HasKey("volume"))
        {
            PlayerPrefs.SetFloat("volume", 1);
        }

        if(PlayerPrefs.GetInt("somOn") == 1)
        {
            AudioListener.volume = PlayerPrefs.GetFloat("volume");
        } else
        {
            AudioListener.volume = 0;
        }
    }

    private void VerifyQuestionVersion()
    {
        string txt = Resources.Load<TextAsset>("Files/questions").ToString();

        using (StreamReader reader = StringToStreamReader(txt))
        {
            string line = reader.ReadLine();
            string[] word = line.Split(';');

            if (int.Parse(word[1]) <= persistence.VerifyQuestionVersion())
            {
                return;
            }
            else
            {
                persistence.DeleteTableQuestion();
                while (!reader.EndOfStream)
                {
                    string linha = reader.ReadLine();
                    string[] palavras = linha.Split(';');
                    persistence.InsertQuestion(new Question(int.Parse(palavras[0]), int.Parse(palavras[1]), palavras[2], palavras[3], palavras[4], int.Parse(palavras[5])));
                }

                persistence.UpdateQuestionVersion(int.Parse(word[1]));
            }
        }
    }

    private void VerifyWordsVersion()
    {
        string txt = Resources.Load<TextAsset>("Files/words").ToString();

        using(StreamReader reader = StringToStreamReader(txt))
        {
            string line = reader.ReadLine();
            string[] word = line.Split(';');

            if (int.Parse(word[1]) <= persistence.VerifyWordVersion())
            {
                reader.Close();
                return;
            }
            else
            {
                persistence.DeleteTableWord();
                while (!reader.EndOfStream)
                {
                    string linha = reader.ReadLine();
                    string[] palavras = linha.Split(';');
                    persistence.InsertWord(new Word(int.Parse(palavras[0]), int.Parse(palavras[1]), palavras[2], palavras[3], palavras[4]));
                }

                persistence.UpdateWordVersion(int.Parse(word[1]));
            }
        }
    }

    private StreamReader StringToStreamReader(string s)
    {
        byte[] array = Encoding.UTF8.GetBytes(s);
        MemoryStream stream = new MemoryStream(array);
        return new StreamReader(stream);
    }

    public void PlayButtonOnClick()
    {
        StartCoroutine(ChangeScene());
    }

    private IEnumerator ChangeScene()
    {
        secondPanel.SetActive(true);
        DateTime initialTime = DateTime.Now;
        VerifyWordsVersion();
        VerifyQuestionVersion();
        int seconds = DateTime.Now.Subtract(initialTime).Seconds;
        if(seconds < 2)
            yield return new WaitForSeconds((2-seconds));

        SceneManager.LoadScene("MenuScene");
    }
}
