﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VocabularyCategoryPanel : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Text text;
    private MenuController menuController;
    private Category category;

    public void Setup(Category c, MenuController mc)
    {
        this.image.sprite = Resources.Load<Sprite>("category/" + c.scholarYear + "Y/Level5/" + c.name);
        this.text.text = c.name;
        this.menuController = mc;
        this.category = c;
    }

    public void ButtonOnClick()
    {
        menuController.SetVocabularyOfCategory(category);
    }
}
