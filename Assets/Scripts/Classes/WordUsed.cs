﻿using SQLite4Unity3d;

public class WordUsed
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public int userId { get; set; }
    public int wordId { get; set; }
    public int categoryId { get; set; }

    public WordUsed()
    {
    }

    public WordUsed(Player user, Word word)
    {
        this.userId = user.Id;
        this.wordId = word.id;
        this.categoryId = word.categoryId;
    }

    public override string ToString()
    {
        return "ID: " + Id + " / User ID: " + userId + " / Word ID: " + wordId;
    }
}
