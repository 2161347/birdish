﻿using SQLite4Unity3d;

public class Level
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public int number { get; set; }
    public int categoryId { get; set; }

    public Level()
    {
    }

    public Level(int level, int categoryId)
    {
        this.number = level;
        this.categoryId = categoryId;
    }

    public Level(int level, Category category)
    {
        this.number = level;
        this.categoryId = category.Id;
    }

    public override string ToString()
    {
        return "Id: " + Id + " / Level: " + number + " / Category: " + categoryId;
    }
}
