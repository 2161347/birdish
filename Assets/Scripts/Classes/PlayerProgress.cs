﻿using SQLite4Unity3d;

public class PlayerProgress
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public int playerId { get; set; }
    public int levelId { get; set; }
    public int score { get; set; }

    public PlayerProgress()
    {
    }

    public PlayerProgress(int playerId, int roundId, int score)
    {
        this.playerId = playerId;
        this.levelId = roundId;
        this.score = score;
    }

    public PlayerProgress(Player user, Level level, int score)
    {
        this.playerId = user.Id;
        this.levelId = level.Id;
        this.score = score;
    }
}