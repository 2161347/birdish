﻿using SQLite4Unity3d;

public class Player
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    [Unique]
    public string name { get; set; }
    public int schoolarYear { get; set; }
    public int coins { get; set; }
    public string avatar { get; set; }

    public Player()
    {
    }

    public Player(string name, int schoolarYear, int coins, string avatar)
    {
        this.name = name;
        this.schoolarYear = schoolarYear;
        this.coins = coins;
        this.avatar = avatar;
    }

    public override string ToString()
    {
        return "Id: " + Id + " / Player: " + name + " / Schoolar Year: " + schoolarYear + " / Coins: " + coins + " / Avatar: "+ avatar;
    }
}

