﻿using System;
using System.Text;
using SQLite4Unity3d;

public class Word
{

    [PrimaryKey, Unique]
    public int id { get; set; }
    public int categoryId { get; set; }
    public string word { get; set; }
    public string palavra { get; set; }
    public string example { get; set; }

    public Word() { }

    public Word(int id, int categoryId, string word, string palavra, string example)
    {
        this.id = id;
        this.categoryId = categoryId;
        this.word = word;
        this.palavra = palavra;
        this.example = example;
    }

    public Word(int id, Category category, string word, string palavra, string example)
    {
        this.id = id;
        this.categoryId = category.Id;
        this.word = word;
        this.palavra = palavra;
        this.example = example;
    }

    public override string ToString()
    {
        return "Id: " + id + " - CategoryId: " + categoryId + " - Word: " + word + " - Palavra: " + palavra + " - Example: " + example;
    }

    public override bool Equals(object obj)
    {
        var w = obj as Word;
        return this.id == w.id;
    }
}
