﻿using System;
using SQLite4Unity3d;

public class Question
{
        [PrimaryKey, Unique]
        public int id { get; set; }
        public int categoryId { get; set; }
        public string question { get; set; }
        public string correct { get; set; }
        public string wrong { get; set; }
        public int type { get; set; }


    public Question() { }

    public Question(int id, int categoryId, string question, string correct, string wrong, int type)
    {
        this.id = id;
        this.categoryId = categoryId;
        this.question = question;
        this.correct = correct;
        this.wrong = wrong;
        this.type = type;
    }

    public Question(int id, Category category, string question, string correct, string wrong, int type)
    {
        this.id = id;
        this.categoryId = category.Id;
        this.question = question;
        this.correct = correct;
        this.wrong = wrong;
        this.type = type;
    }
}
