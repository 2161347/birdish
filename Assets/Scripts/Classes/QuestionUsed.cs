﻿using SQLite4Unity3d;

public class QuestionUsed
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public int userId { get; set; }
    public int questionId { get; set; }
    public int categoryId { get; set; }

    public QuestionUsed()
    {
    }

    public QuestionUsed(Player user, Question question)
    {
        this.userId = user.Id;
        this.questionId = question.id;
        this.categoryId = question.categoryId;
    }

    public override string ToString()
    {
        return "ID: " + Id + " / User ID: " + userId + " / Question ID: " + questionId;
    }
}
