﻿using SQLite4Unity3d;


public class ItemUnlocked
{
    [PrimaryKey, AutoIncrement]
    public int Id { get; set; }
    public string nameObject { get; set; }
    public string nameImage { get; set; }
    public int userId { get; set; }
    public bool isBeingUsed { get; set; }
    public float positionX { get; set; }
    public float positionY { get; set; }

    public ItemUnlocked()
    {
    }

    public ItemUnlocked(string name, int userId)
    {
        this.nameObject = name;
        this.nameImage = null;
        this.userId = userId;
        this.isBeingUsed = false;
        this.positionX = 0;
        this.positionY = 0;

    }

    public ItemUnlocked(string name, Player player)
    {
        this.nameObject = name;
        this.nameImage = null;
        this.userId = player.Id;
        this.isBeingUsed = false;
        this.positionX = 0;
        this.positionY = 0;
    }

    public override string ToString()
    {
        return Id + "/" + nameObject + "/" + nameImage + "/" + isBeingUsed + "/" + positionX + "/" + positionY;
    }

    public string ToStringNameImageNameobject()
    {
        return "Name Tipo: " + nameObject + " / Name da imagem: " + nameImage;
    }
}
