﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessagePanel : MonoBehaviour
{
    public int custoItem { get; set; }
    public GameObject objetoShop { get; set; }

    public MessagePanel(int custoItem, GameObject objetoShop)
    {
        this.custoItem = custoItem;
        this.objetoShop = objetoShop;
    }

    public MessagePanel()
    {
    }
}
