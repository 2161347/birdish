﻿using UnityEngine;
using SQLite4Unity3d;

public class Category {

    [AutoIncrement, PrimaryKey]
    public int Id { get; set; }
    public string name { get; set; }
    public int scholarYear { get; set; }

    public Category()
    {
    }

    public Category(string name, int scholarYear)
    {
        this.name = name;
        this.scholarYear = scholarYear;
    }

    public override string ToString()
    {
        return "Id: " + Id + " / Name: " + name + " / Scholar Year: " + scholarYear;
    }
}