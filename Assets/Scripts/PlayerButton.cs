﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerButton : MonoBehaviour
{
    [SerializeField] private Text btnText;
    [SerializeField] private Image btnImage;
    [SerializeField] private Button deleteButton;
    private Player player;
    private MenuController menuController;


    public void Setup(Player p, MenuController controller)
    {
        this.player = p;
        this.menuController = controller;

        btnText.text = player.name;
        btnImage.sprite = Resources.Load<Sprite>("avatars/" + p.avatar);

        if(player.Id == Persistence.Instance.CurrentPlayer.Id)
        {
            deleteButton.gameObject.SetActive(false);
        } else
        {
            deleteButton.gameObject.SetActive(true);
        }
    }

    public void BtnOnClick()
    {
        menuController.ChangePlayer(player);
    }

    public void DeleteButtonOnClick()
    {
        menuController.ShowMessageDeletePlayer(player.Id, gameObject);
    }
}
