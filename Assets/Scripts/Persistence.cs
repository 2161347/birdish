using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Persistence
{

    private static Persistence instance;

    private Player currentPlayer;
    private DataService dataService;

    public Category category;

    public String avatarChoice;

    private Dictionary<string, List<string>> pairs = new Dictionary<string, List<string>>();

    private Persistence()
    {
        dataService = new DataService("database.db");

        dataService.InitiateDatabase();

        pairs.Add("Hello", new List<string>() { "Good morning", "Good night", "Good evening", "I'm great", "I'm OK", "See you later", "Not so good", "I'm fine", "Wanna play" });
        pairs.Add("Good morning", new List<string>() { "Good night", "Good evening", "I'm great", "I'm OK", "See you later", "Not so good", "Thanks", "How are you", "Wanna play" });
        pairs.Add("Good afternoon", new List<string>() { "Good night", "Good evening", "I'm great", "I'm OK", "See you later", "Not so good", "How are you", "Bye", "Wanna play" });
        pairs.Add("Good evening", new List<string>() { "Good morning", "Good afternoon", "I'm great", "I'm OK", "See you later", "Not so good", "Thanks", "How are you", "Hello", "Bye", "Wanna play" });
        pairs.Add("Good night", new List<string>() { "Good morning", "Good afternoon", "I'm great", "I'm OK", "See you later", "Not so good", "Thanks", "How are you", "Hello", "Bye", "Wanna play" });
        pairs.Add("Bye", new List<string>() { "Good morning", "Good evening", "Good night", "I'm great", "I'm OK", "Not so good", "How are you", "I'm fine", "Wanna play" });
        pairs.Add("See you later", new List<string>() { "Good morning", "Good evening", "Good night", "I'm great", "I'm OK", "Not so good", "How are you", "I'm fine", "Wanna play" });
        pairs.Add("How are you", new List<string>() { "Good morning", "Good evening", "Good night", "Not so good", "See you later", "Thanks", "Bye", "I'm fine", "Wanna play" });
        pairs.Add("I'm OK", new List<string>() { "Good morning", "Good evening", "Good night", "Not so good", "How are you", "Bye", "Thanks", "See you later", "Wanna play" });
        pairs.Add("I'm great", new List<string>() { "Good morning", "Good evening", "Good afternoon", "Good night", "Not so good", "How are you", "Bye", "Thanks", "See you later", "Wanna play" });
        pairs.Add("Not so good", new List<string>() { "Good morning", "Good afternoon", "Good evening", "Good night", "I'm great", "I'm OK", "How are you", "Bye", "Thanks", "Wanna play" });
        pairs.Add("Thanks", new List<string>() { "Good morning", "Good evening", "Good night", "Not so good", "How are you", "Bye", "See you later", "I'm great", "I'm OK", "Wanna play" });
    }

    public List<int> TestIntersect()
    {
        List<int> result = new List<int>();
        for (int i = 0; i < pairs.Count; ++i)
        {
            List<string> lista1 = pairs.ElementAt(i).Value;

            for (int x = i; x < pairs.Count; x++)
            {
                result.Add(lista1.Intersect(pairs.ElementAt(x).Value).ToList().Count);
            }
        }

        return result;
    }

    public static Persistence Instance => instance ?? (instance = new Persistence());

    public bool hasLastLogin()
    {
        return PlayerPrefs.HasKey("PlayerId");
    }

    public int getLastLogin()
    {
        return PlayerPrefs.GetInt("PlayerId");
    }

    public void setLastLogin(int id)
    {
        PlayerPrefs.SetInt("PlayerId", id);
    }

    public IEnumerable<Category> GeAllCategoriesByScholarYear(int scholarYear)
    {
        return dataService.GetCategoriesByScholarYear(scholarYear);
    }

    public IEnumerable GetRoundsByCategory(int categoryId)
    {
        return dataService.GetRoundsByCategory(categoryId);
    }

    public IEnumerable GetAllPlayers()
    {
        return dataService.getAllPlayers();
    }

    public void UpdatePlayer()
    {
        dataService.UpdatePlayer(currentPlayer);
    }

    public void SaveScore(int roundId, int score)
    {
        dataService.SaveScore(currentPlayer.Id, roundId, score);
    }

    public Player SetPlayerBy(int id)
    {
        return currentPlayer = dataService.GetPlayerById(id);
    }

    public Player CurrentPlayer
    {

        get => currentPlayer ?? SetPlayerBy(PlayerPrefs.GetInt("PlayerId"));

        set
        {
            this.currentPlayer = value;
            setLastLogin(value.Id);
        }

    }

    public DataService CurrentDataService
    {

        get => dataService;

    }

    public Dictionary<string, List<string>> Pairs
    {

        get => pairs;

    }

    public bool levelOpen(int level, int id)
    {
        if (level == 1)
        {
            return true;
        }

        Level previousLevel = dataService.GetLevel(level - 1, id);
        return dataService.getPlayerProgress(currentPlayer.Id, previousLevel.Id) != null;
    }

    public List<PlayerProgress> GetPlayerProgresses()
    {
        List<PlayerProgress> p = new List<PlayerProgress>();

        foreach (PlayerProgress pp in dataService.GetAllPlayerProgress())
        {
            p.Add(pp);
        }

        return p;
    }



    public Player GetPlayerById(int id)
    {
        return dataService.GetPlayerById(id);
    }

    public int GetLastLevelOfCategoryDoneByMe(int categoryId)
    {
        try
        {
            return dataService.GetLastLevelOfCategoryDoneByAPlayer(currentPlayer.Id, categoryId);
        }
        catch (NullReferenceException e)
        {
            return 0;
        }

    }

    public IEnumerable<Word> GetWordsOfCategory(Category c)
    {
        return dataService.GetWordsOfCategory(c.Id);
    }

    public IEnumerable<Word> GetWordOfCategoryOrderByWord(Category c)
    {
        return dataService.GetWordsOfgategoryOrderByWord(c.Id);
    }

    public Level GetLevelOfCategoryAndLevel(int categoryId, int level)
    {
        return dataService.GetLevelOfCategoryAndLevel(categoryId, level);
    }



    public void InsertItem(ItemUnlocked item)
    {
        dataService.InsertItemUnlocked(item);
    }

    public void UpdateItem(ItemUnlocked itemMoved)
    {
        dataService.UpdateItem(itemMoved);
    }

    public ItemUnlocked GetItemByName(string nameObject, int userID)
    {
        return dataService.GetItemByName(nameObject, userID);
    }

    public ItemUnlocked GetItemByImageName(int userID, string nameImagem)
    {
        return dataService.GetItemByImageName(userID, nameImagem);
    }

    public IEnumerable<ItemUnlocked> GetItemsByUserID(int userID)
    {
        List<ItemUnlocked> listaItems = new List<ItemUnlocked>();

        foreach (ItemUnlocked i in dataService.GetItemsByUserID(userID))
        {
            listaItems.Add(i);
        }

        return listaItems;
    }

    public IEnumerable<ItemUnlocked> GetItemsBeingUsedByUserID(int userID)
    {
        List<ItemUnlocked> listaItems = new List<ItemUnlocked>();

        foreach (ItemUnlocked i in dataService.GetItemsBeingUsedByUserID(userID))
        {
            listaItems.Add(i);
        }

        return listaItems;
    }

    public List<Word> GetWordsOfCategoryRandom(int categoryId)
    {
        System.Random rnd = new System.Random();
        return dataService.GetWordsOfCategory(categoryId).OrderBy(item => rnd.Next()).ToList();
    }

    public List<Question> GetRandomQuestionOfCategory(int id, int type)
    {
        System.Random rnd = new System.Random();
        return dataService.GetQuestionOfCategory(id, type).OrderBy(item => rnd.Next()).ToList();
    }

    public List<QuestionUsed> GetQuestionsUsedOfCategory(int id)
    {
        return dataService.GetQuestionsUsedOfCategory(id, currentPlayer.Id).ToList();
    }

    public void InsertQuestionUsed(Question q)
    {
        dataService.InserQuestionUsed(q, currentPlayer);
    }

    public void DeleteQuestionUsedOfCategory(int id)
    {
        dataService.DeleteQuestionUsedOfCategory(id, currentPlayer.Id);
    }

    public List<WordUsed> GetWordUsedOfCategory(int id)
    {
        return dataService.GetWordUsedOfCategory(id, currentPlayer.Id).ToList();
    }

    public void DeleteUsedWordsOfCategory(int categoryId)
    {
        dataService.DeleteUsedWordsOfCategory(categoryId, currentPlayer.Id);
    }

    public void InsertWordUsed(Word w)
    {
        dataService.InsertWordUsed(w, currentPlayer);
    }

    public int VerifyWordVersion()
    {
        return dataService.VerifyWordVersion();
    }

    public void InsertWord(Word w)
    {
        dataService.InsertWord(w);
    }

    public void DeleteTableWord()
    {
        dataService.DeleteTableWord();
    }

    public void UpdateWordVersion(int version)
    {
        dataService.UpdateWordVersion(version);
    }

    public int VerifyQuestionVersion()
    {
        return dataService.VerifyQuestionVersion();
    }

    public void InsertQuestion(Question q)
    {
        dataService.InsertQuestion(q);
    }

    public void DeleteTableQuestion()
    {
        dataService.DeleteTableQuestion();
    }

    public void UpdateQuestionVersion(int version)
    {
        dataService.UpdateQuestionVersion(version);
    }

    public void DeleteAllPlayerData(int playerIdToDelete)
    {
        dataService.DeletePlayerProgressOfPlayer(playerIdToDelete);
        dataService.DeleteItemUnlockedOfPlayer(playerIdToDelete);
        dataService.DeleteWordUsedOfPlayer(playerIdToDelete);
        dataService.DeleteQuestionUsedOfPlayer(playerIdToDelete);
        dataService.DeletePlayer(playerIdToDelete);
    }
}