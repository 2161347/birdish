﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    private static float notificationTime = 1f;

    //Aux variables
    private Persistence persistence = Persistence.Instance;
    private DataService dataService;
    private Player updatePlayer;
    private int GradeAuxiliar = 0;
    private Color colorRed;
    private Color colorWhite;
    private List<GameObject> objects = new List<GameObject>();
    private List<GameObject> rankingObjects = new List<GameObject>();
    private Boolean avatarSaved = false;

    //Audio
    [SerializeField] private AudioSource audioSource;

    //Panels
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject ProfilePanel;
    [SerializeField] private GameObject GamePanel;
    [SerializeField] private GameObject SettingsPanel;
    [SerializeField] private GameObject vocabularyPanel;
    [SerializeField] private GameObject editPlayerPanel;

    //Menu Info
    [SerializeField] private Text PageText;
    [SerializeField] private Image menuPanel;
    [SerializeField] private Button profileBtn;
    [SerializeField] private Button gamesBtn;
    [SerializeField] private Button vocabularyBtn;
    [SerializeField] private Button settingsBtn;

    //Profile Panel Info
    [SerializeField] private Text NameDisplayText;
    [SerializeField] private Text CoinsDisplayText;
    [SerializeField] private Image AvatarDoUtilizadorTopLeft;
    [SerializeField] private GameObject RightCostumizeOptionsPanel;
    [SerializeField] private GameObject LeftCostumizeOptionsPanel;
    [SerializeField] private GameObject FooterPanel;
    [SerializeField] private Button RankingButton;
    [SerializeField] private Button AddPlayerButton;
    [SerializeField] private Image dollImage;

    //Game Panel Info
    [SerializeField] private RectTransform CategoryButtonsContent;
    [SerializeField] private GameObject CategoryButtonsPrefab;
    [SerializeField] private GameObject PlayGamesPanel;
    [SerializeField] private Text CategorySubMenuText;
    [SerializeField] private GameObject gameMainPanel;
    [SerializeField] private GameObject bulletsPanel;
    [SerializeField] private Image bulletPrefab;
    [SerializeField] private GameObject endGamePanel;
    [SerializeField] private Text levelCompleteText;
    [SerializeField] private Text coinsText;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject flashCardPrefab;
    [SerializeField] private GameObject dialogCardPrefab;
    [SerializeField] private GameObject imageHotSpotPrefab;
    [SerializeField] private GameObject dragAnddropPrefab;
    [SerializeField] private GameObject dragTheWordsPrefab;
    [SerializeField] private GameObject memoryGamePrefab;
    [SerializeField] private GameObject unscramblingPrefab;
    [SerializeField] private GameObject fillInTheBlanksPrefab;
    [SerializeField] private GameObject dictationPrefab;
    [SerializeField] private GameObject fillInTheBlanksWordsPrefab;
    [SerializeField] private GameObject imageCaptionPrefab;
    [SerializeField] private GameObject wordOrderPrefab;
    [SerializeField] private GameObject multipleChoicePrefab;
    [SerializeField] private GameObject trueFalsePrefab;
    [SerializeField] private GameObject imageHotSpotGamePrefab;
    [SerializeField] private GameObject wrongAnswerPanel;
    [SerializeField] private GameObject correctAnswerPanel;
    private List<GameObject> currentGame;
    private List<Image> roundBullets;
    private int currentDialogCard;
    private List<Word> words;
    private int lastLevel;
    private Category lastCategory;
    private DateTime initialTime;
    private int roundId;
    private int currentNumberGame;
    private List<GameObject> ListObjectsLeft = new List<GameObject>();
    private List<GameObject> ListObjectsRight = new List<GameObject>();
    private List<QuestionUsed> questionUseds;
    private List<WordUsed> wordUseds;
    private bool gameWon = true;

    //Vocabulary Panel
    [SerializeField] private GameObject vocabularyMainPanel;
    [SerializeField] private GameObject categoryPanelVocabularyPrefab;
    [SerializeField] private RectTransform vocabularySubPanel;
    [SerializeField] private GameObject wordPanelVocabulary;
    [SerializeField] private RectTransform wordSubPanelVocabulary;
    [SerializeField] private GameObject wordPrefab;

    //Level Panel Warning
    [SerializeField] private GameObject warningPanel;
    [SerializeField] private Text warningText;
    [SerializeField] private GameObject contentLevelPanel;

    //Settings Panel
    [SerializeField] private GameObject settingsSubPanel;
    [SerializeField] private GameObject choosePlayerPanel;
    [SerializeField] private GameObject playersContentPanel;
    [SerializeField] private GameObject playerBtnPrefab;
    [SerializeField] private Image btnSoundOnImage;
    [SerializeField] private Slider volumeSlider;
    [SerializeField] private Image soundImage;
    private int playerIdToDelete;
    private GameObject objectToRemove;

    //RankingPanel
    [SerializeField] private GameObject rankingPanel;

    //Ranking Panel Info
    [SerializeField] private Image firtsPlayerAvatar;
    [SerializeField] private Text firtsPlayerUsername;
    [SerializeField] private Text firtsPlayerPoints;
    [SerializeField] private GameObject secondPlayerPrefab;
    [SerializeField] private RectTransform rankingPrefabParent;

    //EditPlayerPanel
    [SerializeField] private Button profileAvatar;
    [SerializeField] private InputField profileName;
    [SerializeField] private Button ThirdGradeEditProfile;
    [SerializeField] private Button ForthGradeEditProfile;
    [SerializeField] private GameObject PanelChangeAvatar;
    [SerializeField] private GameObject SubPanelChangeAvatar;
    [SerializeField] private GameObject prefabAvatar;
    [SerializeField] private GameObject NameYearPanel;
    [SerializeField] private Text textThird;
    [SerializeField] private Text textForth;
    [SerializeField] private GameObject ListaObjetosEsquerda;
    [SerializeField] private GameObject ListaObjetosDireita;
    [SerializeField] private GameObject prefabObjetoShop;

    //CreateNewPlayerPanel
    [SerializeField] private Button NewprofileAvatar;
    [SerializeField] private InputField NewprofileName;
    [SerializeField] private Button ThirdGradeNewProfile;
    [SerializeField] private Button ForthGradeNewProfile;
    [SerializeField] private GameObject CreateNewPlayerPanel;
    [SerializeField] private GameObject PanelCreateAvatar;
    [SerializeField] private GameObject SubPanelCreateAvatar;
    [SerializeField] private GameObject SubPanelCreateNameYearPanel;
    [SerializeField] private Text textThirdButton;
    [SerializeField] private Text textForthButton;
    [SerializeField] private GameObject cancelNewPlayerButton;
    private List<ProfileAvatar> allAvatars;
    private List<String> girlObjects;


    //MessagePanel
    [SerializeField] private GameObject messagePanel;
    [SerializeField] private Image messageBox;
    [SerializeField] private Image messageBird;
    [SerializeField] private Text messageText;
    [SerializeField] private Button buyButton;
    [SerializeField] private Button backButton;
    [SerializeField] private Button yesButton;
    [SerializeField] private Button noButton;
    private MessagePanel panelMessageInfo;


    public void Start()
    {
        ColorUtility.TryParseHtmlString("#E95642", out colorRed);
        ColorUtility.TryParseHtmlString("#FFFFFF", out colorWhite);


        if (persistence.hasLastLogin())
        {
            ProfileButtonOnClick();
        } else
        {
            CreateNewPlayerButtonOnClick();
        }
    }



    #region Menu Buttons

    public void ProfileButtonOnClick() 
    {
        RemoveCurrentGame();
        DestroyBullets();

        PageText.text = "Profile";
        menuPanel.sprite = Resources.Load<Sprite>("Menu/Profile");

        ButtonClickSound();

        SetButtonImage(1);

        ProfilePanel.SetActive(true);


        FooterPanel.SetActive(true);

        GamePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(false);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);

        BloquearShop();
        SetupLojaItems();
        SetupItemsAvatar();
        SetPlayerInfo();

        ResetListaObjetos(ListObjectsLeft);
        ResetListaObjetos(ListObjectsRight);
        ListaObjetosDireita.SetActive(false);
        ListaObjetosEsquerda.SetActive(false);

    }

    public void GamesButtonOnClick()
    {
        RemoveCurrentGame();
        DestroyBullets();

        PageText.text = "Games";
        menuPanel.sprite = Resources.Load<Sprite>("Menu/Games");

        ButtonClickSound();

        SetButtonImage(2);

        ProfilePanel.SetActive(false);
        GamePanel.SetActive(true);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(false);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);


        SetCategoriesPanel();
    }

    public void SettingsButtonOnClick()
    {
        RemoveCurrentGame();
        DestroyBullets();

        PageText.text = "Settings";
        menuPanel.sprite = Resources.Load<Sprite>("Menu/Settings");

        ButtonClickSound();

        SetButtonImage(4);

        if (PlayerPrefs.GetInt("somOn") == 0 && btnSoundOnImage.transform.localPosition.x > 0)
        {
            btnSoundOnImage.transform.localPosition = new Vector2(-btnSoundOnImage.transform.localPosition.x, btnSoundOnImage.transform.localPosition.y);
        }

        volumeSlider.value = PlayerPrefs.GetFloat("volume");


        ProfilePanel.SetActive(false);
        GamePanel.SetActive(false);
        SettingsPanel.SetActive(true);
        choosePlayerPanel.SetActive(false);
        settingsSubPanel.SetActive(true);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(false);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);

    }

    public void VocabularyButtonOnClick() 
    {
        RemoveCurrentGame();
        DestroyBullets();

        PageText.text = "Vocabulary";
        menuPanel.sprite = Resources.Load<Sprite>("Menu/vocabulary");

        ButtonClickSound();

        SetButtonImage(3);

        ProfilePanel.SetActive(false);
        GamePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        settingsSubPanel.SetActive(false);
        vocabularyPanel.SetActive(true);
        rankingPanel.SetActive(false);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);

        SetVocabularyCategories();

    }

    public void RankingButtonOnClick()
    {
        PageText.text = "Settings";
        menuPanel.sprite = Resources.Load<Sprite>("Menu/Settings");

        ButtonClickSound();

        ProfilePanel.SetActive(false);
        GamePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        settingsSubPanel.SetActive(false);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(true);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);

        SetRanking();

    }

    private void SetButtonImage(int i)
    {

        ResetMenuButtons();

        switch (i)
        {
            case 1:
                profileBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 180);
            break;

            case 2:
                gamesBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Games_Sleected");
                gamesBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 180);
            break;

            case 3:
                vocabularyBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Vocabulary_Selected");
                vocabularyBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 180);
            break;

            case 4:
                settingsBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Settings_Selected");
                settingsBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 180);
            break;
        }
    }

    private void ResetMenuButtons()
    {
        profileBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 160);

        gamesBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 160);
        gamesBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Games_Deselected");
        
        vocabularyBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 160);
        vocabularyBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Vocabulary_Deselected");

        settingsBtn.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 160);
        settingsBtn.GetComponent<Image>().sprite = Resources.Load<Sprite>("Menu/Settings_Deselected");
    }


    #endregion

    #region Ranking Functions
    private void SetRanking() 
    {

        foreach(GameObject o in rankingObjects)
        {
            Destroy(o);
        }

        rankingObjects.Clear();

        List<PlayerProgress> progresses = persistence.GetPlayerProgresses();

        var prog = progresses.GroupBy(x => x.playerId).OrderByDescending(x => x.Sum(y => y.score));

        if(prog.Count() >= 1)
        {

            Player p = persistence.GetPlayerById(prog.ElementAt(0).Key);
            firtsPlayerPoints.text = prog.ElementAt(0).Sum(x => x.score).ToString();
            firtsPlayerUsername.text = p.name;
            firtsPlayerAvatar.sprite = Resources.Load<Sprite>("avatars/" + p.avatar);

            rankingPrefabParent.sizeDelta = new Vector2(rankingPrefabParent.rect.width, prog.Count() * 450);


            for (int i = 1; i < prog.Count(); i++)
            {

                GameObject rp = Instantiate(secondPlayerPrefab);
                rankingObjects.Add(rp);
                rp.transform.SetParent(rankingPrefabParent, false);
                RankingPanel rankingPanel = rp.GetComponent<RankingPanel>();
                rankingPanel.Setup(persistence.GetPlayerById(prog.ElementAt(i).Key), i + 1, prog.ElementAt(i).Sum(x => x.score));
            }
        } else
        {
            firtsPlayerPoints.text = "0";
            firtsPlayerUsername.text = persistence.CurrentPlayer.name;
            firtsPlayerAvatar.sprite = Resources.Load<Sprite>("avatars/" + persistence.CurrentPlayer.avatar);
        }

    }
    #endregion

    #region Player Functions

    public void ChangePlayerOnClick() {

        ClearObjects();

        ButtonClickSound();

        IEnumerable players = persistence.GetAllPlayers();

        foreach (Player p in players)
        {
            GameObject btn = Instantiate(playerBtnPrefab);
            btn.transform.SetParent(playersContentPanel.transform, false);
            PlayerButton pb = btn.GetComponent<PlayerButton>();
            pb.Setup(p, this);
            objects.Add(btn);
        }

        settingsSubPanel.SetActive(false);
        choosePlayerPanel.SetActive(true);
    }

    public void ChangePlayer(Player p)
    {
        persistence.CurrentPlayer = p;
        SetPlayerInfo();
        ProfileButtonOnClick();
    }

    public void SetPlayerInfo()
    {
        Player currentPlayer = persistence.CurrentPlayer;

        NameDisplayText.text = currentPlayer.name;
        CoinsDisplayText.text = currentPlayer.coins.ToString();

        AvatarDoUtilizadorTopLeft.GetComponent<Image>().sprite = Resources.Load<Sprite>("avatars/" + currentPlayer.avatar);
        dollImage.GetComponent<Image>().sprite = Resources.Load<Sprite>("bodies/" + currentPlayer.avatar);
    }


    #endregion

    #region Games Functions

    public void SetCategoriesPanel()
    {
        ClearObjects();

        IEnumerable categories = persistence.GeAllCategoriesByScholarYear(persistence.CurrentPlayer.schoolarYear);

        int i = 0;

        foreach (Category c in categories)
        {
            GameObject gameObject = Instantiate(CategoryButtonsPrefab);
            objects.Add(gameObject);
            gameObject.transform.SetParent(CategoryButtonsContent, false);
            CategoryPanel categoryPanel = gameObject.GetComponent<CategoryPanel>();
            categoryPanel.Setup(c, this);
            i++;
        }

        CategoryButtonsContent.sizeDelta = new Vector2(CategoryButtonsContent.rect.width, (i / 2 * 500) + (i%2 * 500));
        CategoryButtonsContent.transform.position = new Vector3(0, 0, 0);
    }

    public void SetGamePanel(Category c, int level)
    {
        gameWon = true;

        lastLevel = level;
        lastCategory = c;

        currentGame = new List<GameObject>();
        roundBullets = new List<Image>();

        currentNumberGame = 0;

        CategorySubMenuText.text = c.name;

        PlayGamesPanel.SetActive(true);
        GamePanel.SetActive(false);
        endGamePanel.SetActive(false);

        roundId = persistence.GetLevelOfCategoryAndLevel(c.Id, level).Id;

        questionUseds = persistence.GetQuestionsUsedOfCategory(c.Id);
        wordUseds = persistence.GetWordUsedOfCategory(c.Id);

        switch (level)
        {
            case 1:
                StartLevelOne(c);
                break;
            case 2:
                StartLevelTwo();
                break;
            case 3:
                StartLevelThree();
                break;
            case 4:
                StartLevelFour();
                break;
            default:

                break;
        }
    }

    #region Level One

    private void StartLevelOne(Category c)
    {
        words = persistence.GetWordsOfCategory(c).Cast<Word>().ToList();

        currentDialogCard = 0;

        SetBullets(5);

        SetFlashCard();
    }

    private void SetFlashCard()
    {
        GameObject flashCard = Instantiate(flashCardPrefab);
        FlashCard fc = flashCard.GetComponent<FlashCard>();
        fc.Setup(words, FlashCardNextButtonOnClick);
        flashCard.transform.SetParent(gameMainPanel.transform, false);
        currentGame.Add(flashCard);
    }

    private void FlashCardNextButtonOnClick()
    {
        Destroy(currentGame[0]);
        currentGame.Clear();
        ChangeBulletState(currentNumberGame);
        SetDialogCard();
    }

    private void SetDialogCard()
    {
        List<Word> availableWords = GetUsableWords(words, 1);

        GameObject dialogCard = Instantiate(dialogCardPrefab);
        DialogCard dc = dialogCard.GetComponent<DialogCard>();
        dc.Setup(availableWords[0], this);
        RegisterWordUsed(availableWords[0]);
        dialogCard.transform.SetParent(gameMainPanel.transform, false);
        currentGame.Add(dialogCard);
    }

    public void DialogCardButtonOnClick()
    {
        Destroy(currentGame[0]);
        currentGame.Clear();

        if(currentDialogCard < 3)
        {

            ChangeBulletState(currentNumberGame);
            currentDialogCard++;
            SetDialogCard();
        } else
        {
            SetImageHotSpot();
        }
    }

    private void SetImageHotSpot()
    {
        List<Word> availableWords = GetUsableWords(words, 2);

        GameObject hotSpot = Instantiate(imageHotSpotPrefab);
        ImageHotSpot ihs = hotSpot.GetComponent<ImageHotSpot>();
        ihs.Setup(availableWords.GetRange(0,2), this);
        RegisterWordUsed(availableWords[0]);
        RegisterWordUsed(availableWords[1]);
        hotSpot.transform.SetParent(gameMainPanel.transform, false);
        currentGame.Add(hotSpot);
    }

    public void ImageHotSpotButtonOnClick()
    {
        Destroy(currentGame[0]);
        currentGame.Clear();
        ChangeBulletState(currentNumberGame);
        EndGame();
    }

    #endregion

    #region Level Two

    private void StartLevelTwo()
    {

        SetBullets(5);

        InitiateTimer();

        int firstGame = UnityEngine.Random.Range(0, 2);

        if(firstGame < 1)
        {
            SetDragAndDropGame();
        } else
        {
            SetDragTheWordsGame();
        }
    }

    private void SetDragTheWordsGame()
    {
        if(currentNumberGame < 4)
        {
            GameObject dtw = Instantiate(dragTheWordsPrefab);
            DragTheWords dragTheWords = dtw.GetComponent<DragTheWords>();

            List<Word> words = persistence.GetWordsOfCategoryRandom(lastCategory.Id);
            List<Word> availableWords = GetUsableWords(words, 6);
            RegisterWordUsed(availableWords[0]);
            RegisterWordUsed(availableWords[1]);

            dragTheWords.Setup(availableWords, this);
            dtw.transform.SetParent(gameMainPanel.transform, false);
            currentGame.Add(dtw);
        } else
        {
            SetMemoryGame();
        }
    }

    public void DragTheWordsTerminated()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetDragAndDropGame));
    }

    private void SetDragAndDropGame()
    {
        if (currentNumberGame < 4)
        {
            GameObject dad = Instantiate(dragAnddropPrefab);
            dad.transform.SetParent(gameMainPanel.transform, false);
            DragAndDropGame dragAndDrop = dad.GetComponent<DragAndDropGame>();

            List<Word> words = persistence.GetWordsOfCategoryRandom(lastCategory.Id);
            List<Word> availableWords = GetUsableWords(words, 3);
            RegisterWordUsed(availableWords[0]);

            dragAndDrop.Setup(availableWords, this);
            currentGame.Add(dad);
        } else
        {
            SetMemoryGame();
        }
    }

    public void DragAndDropCorrectBtnOnClick()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetDragTheWordsGame));
    }

    public void DragAndDropIncorrectBtnOnClick()
    {
        gameWon = false;
        ChangeBulletState(currentNumberGame);
        WrongAnswerPanelShow();
        StartCoroutine(WrongAnswerPanelHide(notificationTime, SetDragTheWordsGame));
    }

    private void SetMemoryGame()
    {
        GameObject mg = Instantiate(memoryGamePrefab);
        mg.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = persistence.GetWordsOfCategoryRandom(lastCategory.Id);
        List<Word> availableWords = GetUsableWords(words, 6).GetRange(0,6);
        foreach(Word w in availableWords)
        {
            RegisterWordUsed(w);
        }
        mg.GetComponent<MemoryGame>().Setup(this, availableWords);
        currentGame.Add(mg);
    }

    public void MemoryGameTerminated()
    {
        ChangeBulletState(currentNumberGame);
        EndGame();
    }

    #endregion

    #region Level Three

    private void StartLevelThree()
    {
        SetBullets(5);

        InitiateTimer();

        SetUnscramblingGame();
    }

    private void SetUnscramblingGame()
    {
        GameObject u = Instantiate(unscramblingPrefab);
        u.transform.SetParent(gameMainPanel.transform, false);
        Word word = GetUsableWords(persistence.GetWordsOfCategoryRandom(lastCategory.Id).Where(x => x.word.Length < 14).ToList(),1).First();
        RegisterWordUsed(word);
        u.GetComponent<UnscramblingGame>().Setup(this, word);
        currentGame.Add(u);
    }

    public void UnscramblingTerminated()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetMultipleChoiceLevel3));
    }

    private void SetMultipleChoiceLevel3()
    {
        GameObject g = Instantiate(multipleChoicePrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        Question q = GetUsableQuestions(persistence.GetRandomQuestionOfCategory(lastCategory.Id, 1)).First();
        RegisterQuestionUsed(q);
        g.GetComponent<MultipleChoiceGame>().Setup(lastLevel, q, this);
        currentGame.Add(g);

    }

    public void MultipleChoiceCorrectClickLevel3()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(2, SetFillInTheBlanks));
    }

    public void MultipleChoiceInCorrectClickLevel3()
    {
        gameWon = false;
        ChangeBulletState(currentNumberGame);
        WrongAnswerPanelShow();
        StartCoroutine(WrongAnswerPanelHide(notificationTime, SetFillInTheBlanks));
    }

    private void SetImageHotSpotGame()
    {
        GameObject g = Instantiate(imageHotSpotGamePrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = persistence.GetWordsOfCategoryRandom(lastCategory.Id);
        List<Word> availableWords = GetUsableWords(words, 4).GetRange(0, 4);
        foreach(Word w in availableWords)
        {
            RegisterWordUsed(w);
        }
        g.GetComponent<ImageHotspotGame>().Setup(availableWords, this);
        currentGame.Add(g);
    }

    public void ImageHotSpotCorrectClick()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetDictationGameLevel3));
    }

    public void ImageHotSpotIncorrectClick()
    {
        gameWon = false;
        ChangeBulletState(currentNumberGame);
        WrongAnswerPanelShow();
        StartCoroutine(WrongAnswerPanelHide(notificationTime, SetDictationGameLevel3));
    }

    private void SetFillInTheBlanks()
    {
        GameObject g = Instantiate(fillInTheBlanksPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = GetUsableWords(persistence.GetWordsOfCategoryRandom(lastCategory.Id).Where(x => x.word.Length < 14).ToList(), 1);
        RegisterWordUsed(words[0]);
        g.GetComponent<FillInTheBlanksGame>().Setup(this, words[0]);
        currentGame.Add(g);
    }

    public void FillInTheBlanksTerminated()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetImageHotSpotGame));
    }

    private void SetDictationGameLevel3()
    {
        GameObject g = Instantiate(dictationPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = GetUsableWords(persistence.GetWordsOfCategoryRandom(lastCategory.Id), 1);
        RegisterWordUsed(words[0]);
        g.GetComponent<DictationGame>().Setup(lastLevel, this, words[0]);
        currentGame.Add(g);
    }

    public void DictationGameTerminatedLevel3()
    {
        ChangeBulletState(currentNumberGame);
        EndGame();
    }

    #endregion

    #region Level Four

    private void StartLevelFour()
    {

        SetBullets(6);
        InitiateTimer();

        SetMultipleChoiceLevel4();
    }

    private void SetMultipleChoiceLevel4()
    {
        GameObject g = Instantiate(multipleChoicePrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        Question q = GetUsableQuestions(persistence.GetRandomQuestionOfCategory(lastCategory.Id, 2)).First();
        RegisterQuestionUsed(q);
        g.GetComponent<MultipleChoiceGame>().Setup(lastLevel, q, this);
        currentGame.Add(g);

    }

    public void MultipleChoiceCorrectClickLevel4()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetFillInTheBlanksWord));
    }

    public void MultipleChoiceInCorrectClickLevel4()
    {
        gameWon = false;
        ChangeBulletState(currentNumberGame);
        WrongAnswerPanelShow();
        StartCoroutine(WrongAnswerPanelHide(notificationTime, SetFillInTheBlanksWord));
    }

    private void SetWordOrderGame()
    {
        GameObject g = Instantiate(wordOrderPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        Question q = GetUsableQuestions(persistence.GetRandomQuestionOfCategory(lastCategory.Id, 1)).First();
        RegisterQuestionUsed(q);
        g.GetComponent<WordOrderGame>().Setup(this, q);
        currentGame.Add(g);
    }

    public void WordOrderTerminated(int max, int number)
    {
        if(number >= max)
        {
            ChangeBulletState(currentNumberGame);
            CorrectAnswerPanelShow();
            StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetDictationLevel4));
        } else
        {
            gameWon = false;
            ChangeBulletState(currentNumberGame);
            WrongAnswerPanelShow();
            StartCoroutine(WrongAnswerPanelHide(notificationTime, SetDictationLevel4));
        }

    }

    private void SetDictationLevel4()
    {
        GameObject g = Instantiate(dictationPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = GetUsableWords(persistence.GetWordsOfCategoryRandom(lastCategory.Id), 1);
        RegisterWordUsed(words[0]);
        g.GetComponent<DictationGame>().Setup(lastLevel, this, words[0]);
        currentGame.Add(g);
    }

    public void DictationGameTerminatedLevel4()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetTrueOrFalseGame));
    }

    private void SetTrueOrFalseGame()
    {
        GameObject g = Instantiate(trueFalsePrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        Question q = GetUsableQuestions(persistence.GetRandomQuestionOfCategory(lastCategory.Id, 1)).First();
        RegisterQuestionUsed(q);
        g.GetComponent<TrueOrFalseGame>().Setup(q, this);
        currentGame.Add(g);
    }

    public void TrueFalseCorrectOnClick()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, EndGame));
    }

    public void TrueFalseIncorrectOnClick()
    {
        gameWon = false;
        ChangeBulletState(currentNumberGame);
        WrongAnswerPanelShow();
        StartCoroutine(WrongAnswerPanelHide(notificationTime, EndGame));
    }

    private void SetImageCaption()
    {
        GameObject g = Instantiate(imageCaptionPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        List<Word> words = GetUsableWords(persistence.GetWordsOfCategoryRandom(lastCategory.Id), 1);
        RegisterWordUsed(words[0]);
        g.GetComponent<ImageCaptionGame>().Setup(this, words[0]);
        currentGame.Add(g);
    }

    public void ImageCaptionCorrect()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetWordOrderGame));
    }

    private void SetFillInTheBlanksWord()
    {
        GameObject g = Instantiate(fillInTheBlanksWordsPrefab);
        g.transform.SetParent(gameMainPanel.transform, false);
        Question q = GetUsableQuestions(persistence.GetRandomQuestionOfCategory(lastCategory.Id, 3)).First();
        RegisterQuestionUsed(q);
        g.GetComponent<FillinTheBlanksWordsGame>().Setup(this, q);
        currentGame.Add(g);
    }

    public void FillInTheBlanksWordCorrect()
    {
        ChangeBulletState(currentNumberGame);
        CorrectAnswerPanelShow();
        StartCoroutine(CorrectAnswerPanelHide(notificationTime, SetImageCaption));
    }

    #endregion

    #region EndGame

    private void EndGame()
    {
        RemoveCurrentGame();
        DestroyBullets();
        endGamePanel.SetActive(true);

        audioSource.PlayOneShot((AudioClip)Resources.Load("Audio/victoryEndGame"));

        if(lastLevel == 1)
        {
            SavePlayerProgress();
        } else
        {
            SavePlayerProgressWithTime(DateTime.Now);
        }

        SetPlayerInfo();
    }

    public void ReplayGameButtonOnClick()
    {
        SetGamePanel(lastCategory, lastLevel);
    }

    public void NextLevelButtonOnClick()
    {
        int nextLevel = lastLevel;
        if(lastLevel < 4)
        {
            nextLevel++;
        }

        SetGamePanel(lastCategory, nextLevel);
    }

    private void SavePlayerProgress()
    {
        persistence.SaveScore(roundId, 100);
        persistence.CurrentPlayer.coins += 10;
        persistence.UpdatePlayer();

        ShowLevelCompleteAndCoins(10);

    }


    private void SavePlayerProgressWithTime(DateTime finalTime)
    {
        double timeSpent = Math.Round(finalTime.Subtract(initialTime).TotalSeconds);
        CalculatePoints(timeSpent);

    }

    private void ShowLevelCompleteAndCoins(int coins)
    {
        if (gameWon)
        {
            levelCompleteText.text = "Level " + lastLevel + "\nComplete!";
            nextButton.SetActive(true);
        } else
        {
            levelCompleteText.text = "Level " + lastLevel + "\nIncomplete!";
            nextButton.SetActive(false);
        }

        coinsText.text = coins.ToString();
    }

    private void CalculatePoints(double timeSpent)
    {
        int coins = 0;
        int score = 0;

        score = 100;

        if (timeSpent > 90)
        {
            score = (int)Math.Round((score - timeSpent + 90) / 10) * 10;
        }
        if (timeSpent > 180)
        {
            score = 10;
        }

        coins = score / 10;

        if (gameWon)
        {
            persistence.SaveScore(roundId, score);
        }

        persistence.CurrentPlayer.coins += coins;
        persistence.UpdatePlayer();

        ShowLevelCompleteAndCoins(coins);
    }

    #endregion

    private List<Word> GetUsableWords(List<Word> allWords, int minLength)
    {
        List<WordUsed> usedWords = persistence.GetWordUsedOfCategory(lastCategory.Id);
        List<Word> availableWord = new List<Word>();

        foreach(Word w in allWords)
        {
            if (usedWords.Where(x => x.wordId == w.id).ToList().Count == 0)
            {
                availableWord.Add(w);
            }
        }

        if(availableWord.Count < minLength)
        {
            persistence.DeleteUsedWordsOfCategory(lastCategory.Id);
            return allWords;
        }

        return availableWord;
    }

    private void RegisterWordUsed(Word w)
    {
        persistence.InsertWordUsed(w);
    }

    private List<Question> GetUsableQuestions(List<Question> allQuestions)
    {
        List<QuestionUsed> usedQuestions = persistence.GetQuestionsUsedOfCategory(lastCategory.Id);
        List<Question> availableQuestions = new List<Question>();

        foreach(Question q in allQuestions)
        {
            if(usedQuestions.Where(x => x.questionId == q.id).ToList().Count == 0)
            {
                availableQuestions.Add(q);
            }
        }

        if(availableQuestions.Count <= 0)
        {
            persistence.DeleteQuestionUsedOfCategory(lastCategory.Id);
            return allQuestions;
        }

        return availableQuestions;
    }

    private void RegisterQuestionUsed(Question q)
    {
        persistence.InsertQuestionUsed(q);
    }

    private void InitiateTimer()
    {
        initialTime = DateTime.Now;
    }

    private void RemoveCurrentGame()
    {
        if (currentGame != null)
        {
            foreach (GameObject g in currentGame)
            {
                Destroy(g);
            }
            currentGame.Clear();
        }
    }

    private void SetBullets(int number)
    {
        for(int i = 0; i < number; i++)
        {
            Image g = Instantiate(bulletPrefab);
            g.transform.SetParent(bulletsPanel.transform, false);
            roundBullets.Add(g);
        }
    }

    private void ChangeBulletState(int i)
    {
        try
        {
            roundBullets.ElementAt(i).sprite = Resources.Load<Sprite>("Cleared_Level");
            currentNumberGame++;
        } catch(Exception e)
        {
        }
    }

    private void DestroyBullets()
    {
        if (roundBullets != null)
        {
            foreach (Image i in roundBullets)
            {
                Destroy(i.gameObject);
            }
            roundBullets.Clear();
        }
    }

    private void CorrectAnswerPanelShow()
    {
        correctAnswerPanel.SetActive(true);
        audioSource.PlayOneShot((AudioClip) Resources.Load("Audio/correct"));
    }

    private IEnumerator CorrectAnswerPanelHide(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);

        correctAnswerPanel.SetActive(false);
        RemoveCurrentGame();
        action();
    }

    private void WrongAnswerPanelShow()
    {
        wrongAnswerPanel.SetActive(true);
        audioSource.PlayOneShot((AudioClip)Resources.Load("Audio/incorrect"));
    }

    private IEnumerator WrongAnswerPanelHide(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        wrongAnswerPanel.SetActive(false);
        RemoveCurrentGame();
        action();
    }
    #endregion

    #region Settings Functions

    public void SoundBtnOnClick() 
    {
        AudioListener.volume = AudioListener.volume == 0 ? PlayerPrefs.GetFloat("volume") : 0;
        PlayerPrefs.SetInt("somOn", 1 - PlayerPrefs.GetInt("somOn"));

        if(AudioListener.volume == 0)
        {
            soundImage.sprite = Resources.Load<Sprite>("Settings/No_Sound_Icon");
            volumeSlider.value = 0;
        } else
        {
            btnSoundOnImage.transform.localPosition = new Vector2(-btnSoundOnImage.transform.localPosition.x, btnSoundOnImage.transform.localPosition.y);
        }

    }

    public void VolumeSliderOnValueChanged(Slider slider)
    {
        if(slider.value == 0)
        {
            PlayerPrefs.SetInt("somOn", 0);
            btnSoundOnImage.transform.localPosition = new Vector2(-Math.Abs(btnSoundOnImage.transform.localPosition.x), btnSoundOnImage.transform.localPosition.y);
            soundImage.sprite = Resources.Load<Sprite>("Settings/No_Sound_Icon");
        } else
        {
            PlayerPrefs.SetInt("somOn", 1);
            btnSoundOnImage.transform.localPosition = new Vector2(Math.Abs(btnSoundOnImage.transform.localPosition.x), btnSoundOnImage.transform.localPosition.y);
            soundImage.sprite = Resources.Load<Sprite>("Settings/Sound_Icon");
        }

        PlayerPrefs.SetFloat("volume", slider.value);
        if (PlayerPrefs.GetInt("somOn") == 1)
        {
            AudioListener.volume = slider.value;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }


    #endregion

    #region Vocabulary Functions

    private void SetVocabularyCategories()
    {
        vocabularyMainPanel.SetActive(true);
        wordPanelVocabulary.SetActive(false);

        ClearObjects();

        List<Category> categories = persistence.GeAllCategoriesByScholarYear(persistence.CurrentPlayer.schoolarYear).ToList();

        int i = 0;

        foreach (Category c in categories)
        {
            GameObject gameObject = Instantiate(categoryPanelVocabularyPrefab);
            objects.Add(gameObject);
            gameObject.transform.SetParent(vocabularySubPanel.transform, false);
            gameObject.GetComponent<VocabularyCategoryPanel>().Setup(c, this);
            i++;
        }

        vocabularySubPanel.sizeDelta = new Vector2(vocabularySubPanel.rect.width, (i / 2 * 500) + (i % 2 * 500));
        vocabularySubPanel.transform.position = new Vector3(0, 0, 0);
    }

    public void SetVocabularyOfCategory(Category c)
    {
        List<Word> words;
        if (c.Id == 2 || c.Id == 15 || c.Id == 16 || c.Id == 17 || c.Id == 18){
            words = persistence.GetWordsOfCategory(c).ToList();
        } else
        {
            words = persistence.GetWordOfCategoryOrderByWord(c).ToList();
        }


        vocabularyMainPanel.SetActive(false);
        wordPanelVocabulary.SetActive(true);

        foreach(Word w in words)
        {
            GameObject wp = Instantiate(wordPrefab);
            objects.Add(wp);
            wp.GetComponent<WordPrefab>().Setup(w, this);
            wp.transform.SetParent(wordSubPanelVocabulary.transform, false);
        }

        wordSubPanelVocabulary.sizeDelta = new Vector2(wordSubPanelVocabulary.rect.width, (510 * words.Count));
    }

    #endregion

    #region Edit PLayer Functions

    public void EditProfileBtnOnClick()
    {
        PanelChangeAvatar.SetActive(false);
        NameYearPanel.SetActive(true);
        updatePlayer = persistence.CurrentPlayer;
        try
        {
            if(persistence.avatarChoice != "" && avatarSaved){
                profileAvatar.GetComponent<Image>().sprite = Resources.Load<Sprite>("avatars/" + persistence.avatarChoice);
            }
            else
            {
                profileAvatar.GetComponent<Image>().sprite = Resources.Load<Sprite>("avatars/" + updatePlayer.avatar);
            }
        }
        catch{}
         
        profileName.text = updatePlayer.name;

        PageText.text = "Profile";

        int grade = GradeAuxiliar != 0 ? GradeAuxiliar : updatePlayer.schoolarYear;


        if (grade == 3)
        {
            changeGradeToThird();
        }
        else {
            changeGradeToForth();
        }

        ProfilePanel.SetActive(false);
        GamePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        settingsSubPanel.SetActive(false);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(false);
        editPlayerPanel.SetActive(true);
    }



    public void changeGradeToThird()
    {
        GradeAuxiliar = 3;
        ThirdGradeEditProfile.GetComponent<Image>().color = colorRed;
        textThird.color = colorWhite;

        ForthGradeEditProfile.GetComponent<Image>().color = colorWhite;
        textForth.color = colorRed;
    }

    public void changeGradeToForth()
    {
        GradeAuxiliar = 4;
        ThirdGradeEditProfile.GetComponent<Image>().color = colorWhite;
        textThird.color = colorRed;

        ForthGradeEditProfile.GetComponent<Image>().color = colorRed;
        textForth.color = colorWhite;
    }

    public void ClickSave()
    {
        DataService dataService = persistence.CurrentDataService;
        Player updatePlayer = persistence.CurrentPlayer;

        if(profileName.text.Trim() == "")
        {
            ShowMessage(true, "Fill your name.");
            return;
        }

        Player nomeAtualizar = dataService.GetPlayerByName(profileName.text.Trim());

        if (nomeAtualizar == null || nomeAtualizar.Id == updatePlayer.Id)
        {
            if (avatarSaved)
            {
                updatePlayer.avatar = persistence.avatarChoice;
            }
            updatePlayer.name = profileName.text;
            updatePlayer.schoolarYear = GradeAuxiliar;
            dataService.UpdatePlayer(updatePlayer);
            persistence.CurrentPlayer = updatePlayer;
            SetPlayerInfo();
            ProfileButtonOnClick();
        }
        else
        {
            ShowMessage(true, "Name is already used.");
        }

        GradeAuxiliar = 0;
        avatarSaved = false;
        persistence.avatarChoice = "";
    }

    public void ClickdSaveChangesAvatar()
    {
        if(persistence.avatarChoice != "")
        {
            avatarSaved = true;
            EditProfileBtnOnClick();
        } else if (updatePlayer.avatar != null)
        {
            persistence.avatarChoice = updatePlayer.avatar;
            avatarSaved = true;
            EditProfileBtnOnClick();
        }
        else
        {
            ShowMessage(true, "Choose an avatar.");
        }

    }

    public void ClickCancelChangesAvatar()
    {
        avatarSaved = false;
        persistence.avatarChoice = "";
        EditProfileBtnOnClick();
    }

    public void ClickCancelChanges()
    {
        avatarSaved = false;
        persistence.avatarChoice = "";
        GradeAuxiliar = 0;
        ProfileButtonOnClick();
    }



    public void ClearAvatarSelection()
    {
        foreach(var avatar in allAvatars)
        {
            avatar.Deselect();
        }
    }


    public void OpenAvatarPanel()
    {
        ClearObjects();
        NameYearPanel.SetActive(false);
        allAvatars = new List<ProfileAvatar>();
        if (PanelChangeAvatar != null)
        {
            PanelChangeAvatar.SetActive(true);

            for (int i = 1; i <= 12; i++)
            {
                GameObject avatar = Instantiate(prefabAvatar);
                avatar.transform.SetParent(SubPanelChangeAvatar.transform, false);
                ProfileAvatar profileAvatar = avatar.GetComponent<ProfileAvatar>();

                profileAvatar.Setup(i, this);

                if(persistence.avatarChoice != "" && persistence.avatarChoice != null)
                {
                    if(persistence.avatarChoice == "avt" + i)
                        profileAvatar.SetupEdit(i, this);
                } else
                {
                    if(persistence.CurrentPlayer.avatar == "avt" + i)
                        profileAvatar.SetupEdit(i, this);
                }


                objects.Add(avatar);
                allAvatars.Add(profileAvatar);
            }
        }
    }

    public void OpenShopItemLeft(GameObject itemShop)
    {
        if (ListaObjetosEsquerda.activeSelf)
        {
            Image[] listaFilhos = ListaObjetosEsquerda.GetComponentsInChildren<Image>();
            if (itemShop.name == listaFilhos[1].name)
            {
                ListaObjetosEsquerda.SetActive(false);
                ResetListaObjetos(ListObjectsLeft);
            }
            else
            {
                OpenShopItem(itemShop, ListaObjetosEsquerda, ListObjectsLeft);
            }
        }
        else
        {
            OpenShopItem(itemShop, ListaObjetosEsquerda, ListObjectsLeft);
        }
    }

    public void OpenShopItemRight(GameObject itemShop)
    {
        if (ListaObjetosDireita.activeSelf)
        {
            Image[] listaFilhos = ListaObjetosDireita.GetComponentsInChildren<Image>();
            if (itemShop.name == listaFilhos[1].name)
            {
                ListaObjetosDireita.SetActive(false);
                ResetListaObjetos(ListObjectsRight);
            }
            else
            {
                OpenShopItem(itemShop, ListaObjetosDireita, ListObjectsRight);
            }
        }
        else
        {
            OpenShopItem(itemShop, ListaObjetosDireita, ListObjectsRight);
        }
    }

    public void OpenShopItem(GameObject itemShop, GameObject parent, List<GameObject> listaObjetos)
    {
        parent.SetActive(true);
        ResetListaObjetos(listaObjetos);

        if (parent != null)
        {
            ArrayList listaObjetostotais = new ArrayList(Resources.LoadAll("items/" + itemShop.name));
            int numObjetos = listaObjetostotais.Count / 2;


            for (int i = 1; i <= numObjetos; i++)
            {
                GameObject ObjetoShop = Instantiate(prefabObjetoShop);
                ObjetoShop.transform.SetParent(parent.transform, false);

                ItemToDragShop item = ObjetoShop.GetComponentInChildren<ItemToDragShop>();

                item.Setup(i, dollImage, itemShop.name);

                listaObjetos.Add(ObjetoShop);
            }
        }
    }

 

    public void UnlockItem(GameObject objetoShop)
    {
        int coinsUtilizador = persistence.CurrentPlayer.coins;
        int custoItem = int.Parse(objetoShop.GetComponentInChildren<Text>().text);
        if (coinsUtilizador < custoItem)
        {
            ShowMessage(true, "Not enough coins to buy this item");
        }
        else
        {
            panelMessageInfo = new MessagePanel(custoItem, objetoShop);
            ShowMessage(false, "Do you want to buy this item ?");
        }

    }

    public void BloquearShop()
    {
        List<Transform> objetosLeft = new List<Transform>();
        for (int i = 0; i < LeftCostumizeOptionsPanel.gameObject.transform.childCount; i++)
        {
            objetosLeft.Add(LeftCostumizeOptionsPanel.gameObject.transform.GetChild(i));
        }

        List<Transform> objetosRight = new List<Transform>();
        for (int i = 0; i < RightCostumizeOptionsPanel.gameObject.transform.childCount; i++)
        {
            objetosRight.Add(RightCostumizeOptionsPanel.gameObject.transform.GetChild(i));
        }

        Button[] listaBotoes;

        foreach (var filho in objetosLeft)
        {
            listaBotoes = filho.GetComponentsInChildren<Button>(true);

            listaBotoes.First().GetComponent<Button>().enabled = false;
            listaBotoes.Last().gameObject.SetActive(true);
        }

        foreach (var filho in objetosRight)
        {
            listaBotoes = filho.GetComponentsInChildren<Button>(true);

            listaBotoes.First().GetComponent<Button>().enabled = false;
            listaBotoes.Last().gameObject.SetActive(true);
        }

    }


    public void SetupLojaItems()
    {
        IEnumerable<ItemUnlocked> listaItemsDesbloqueados = persistence.GetItemsByUserID(persistence.CurrentPlayer.Id);

        List<Transform> objetosLeft = new List<Transform>();
        for (int i = 0; i < LeftCostumizeOptionsPanel.gameObject.transform.childCount; i++)
        {
            objetosLeft.Add(LeftCostumizeOptionsPanel.gameObject.transform.GetChild(i));
        }

        List <Transform> objetosRight = new List <Transform>();
        for (int i = 0; i < RightCostumizeOptionsPanel.gameObject.transform.childCount; i++)
        {
            objetosRight.Add(RightCostumizeOptionsPanel.gameObject.transform.GetChild(i));
        }



        Button[] listaBotoes;
        bool skip = false;

        foreach (var item in listaItemsDesbloqueados)
        {
            skip = false;

            foreach (var filho in objetosLeft)
            {
                if (item.nameObject == filho.name)
                {

                    listaBotoes = filho.GetComponentsInChildren<Button>();
                    if (listaBotoes.Count() != 1)
                    {
                        listaBotoes.First().GetComponent<Button>().enabled = true;
                        filho.GetComponentsInChildren<Button>().Last().gameObject.SetActive(false); 
                    }
                    skip = true;
                    break;
                }
                
            }

            if (!skip)
            {
                foreach (var filho in objetosRight)
                {

                    if (item.ToString().Contains(filho.name))
                    {

                        listaBotoes = filho.GetComponentsInChildren<Button>();
                        if (listaBotoes.Count() != 1)
                        {
                            listaBotoes.First().GetComponent<Button>().enabled = true;
                            filho.GetComponentsInChildren<Button>().Last().gameObject.SetActive(false);
                        }
                        break;
                    }
                }
            }
            
        }
    }

    public void SetupItemsAvatar()
    {

        int childs = dollImage.transform.childCount;
        for (int i = childs - 1; i >= 0; i--)
        {
            Destroy(dollImage.transform.GetChild(i).gameObject);
        }

        IEnumerable <ItemUnlocked> listaItemsDesbloqueados = persistence.GetItemsBeingUsedByUserID(persistence.CurrentPlayer.Id);

        foreach (var item in listaItemsDesbloqueados)
        {

            GameObject ObjetoShop = Instantiate(prefabObjetoShop);

            ItemToDragShop itemToDrag = ObjetoShop.GetComponentInChildren<ItemToDragShop>();
            Vector3 originalPos = itemToDrag.transform.position;
            itemToDrag.transform.SetParent(dollImage.transform);
            itemToDrag.transform.position = originalPos;
            Destroy(ObjetoShop);


            itemToDrag.SetupOnAvatarImage(item, dollImage);
        }
    }

    #endregion

    #region Create New PLayer Functions

    public void CreateNewPlayerButtonOnClick()
    {
        CreateNewPlayerPanel.SetActive(true);
        PanelCreateAvatar.SetActive(true);
        SubPanelCreateNameYearPanel.SetActive(false);
        updatePlayer = persistence.CurrentPlayer;
        PageText.text = "Create Profile";

        updatePlayer = new Player();

        if (persistence.hasLastLogin())
        {
            cancelNewPlayerButton.SetActive(true);
        } else
        {
            SetMenuButtonsInteractable(false);
        }

        ClearObjects();
        if (PanelCreateAvatar != null)
        {
            allAvatars = new List<ProfileAvatar>();
            SubPanelCreateAvatar.SetActive(true);

            for (int i = 1; i <= 12; i++)
            {
                GameObject avatar = Instantiate(prefabAvatar);
                avatar.transform.SetParent(SubPanelCreateAvatar.transform, false);
                ProfileAvatar profileAvatar = avatar.GetComponent<ProfileAvatar>();
                profileAvatar.Setup(i, this);
                objects.Add(avatar);
                allAvatars.Add(profileAvatar);

            }
        }

        ProfilePanel.SetActive(false);
        GamePanel.SetActive(false);
        SettingsPanel.SetActive(false);
        choosePlayerPanel.SetActive(false);
        settingsSubPanel.SetActive(false);
        vocabularyPanel.SetActive(false);
        rankingPanel.SetActive(false);
        editPlayerPanel.SetActive(false);
        CreateNewPlayerPanel.SetActive(true);
    }

    public void atualizarSelected(string avatarSelected)
    {
        foreach (ProfileAvatar child in SubPanelChangeAvatar.GetComponentsInChildren<ProfileAvatar>())
        {
            if (child.GetComponent<Image>().name != avatarSelected)
            {
                child.GetComponent<Image>().sprite = Resources.Load<Sprite>("Profile/Profile_Icons");
            }
        }
    }

    public void ClickSaveCreateProfile()
    {
        DataService dataService = persistence.CurrentDataService;

        if(NewprofileName.text.Trim() == "")
        {
            ShowMessage(true, "Fill in the name.");
            return;
        }

        if(updatePlayer.schoolarYear != 3 && updatePlayer.schoolarYear != 4)
        {
            ShowMessage(true, "Choose a scholar year.");
            return;
        }

        updatePlayer.avatar = persistence.avatarChoice;
        updatePlayer.name = NewprofileName.text.Trim();
        updatePlayer.coins = 50;
        try
        {
            dataService.InsertPlayer(updatePlayer);
            persistence.CurrentPlayer = updatePlayer;
            persistence.InsertItem(new ItemUnlocked("mochila", persistence.CurrentPlayer.Id));
            SetPlayerInfo();
            ProfileButtonOnClick();
            SetMenuButtonsInteractable(true);
        } catch
        {
            ShowMessage(true, "Name is already used.");
        }

    }

    public void ClickdSaveChangesCreateAvatar()
    {
        if(persistence.avatarChoice != null)
        {
            avatarSaved = true;
            OpenNewProfileNameGrade();
        } else
        {
            ShowMessage(true, "Choose an avatar.");
        }

        ThirdGradeNewProfile.GetComponent<Image>().color = colorWhite;
        textThirdButton.color = colorRed;
        ForthGradeNewProfile.GetComponent<Image>().color = colorWhite;
        textForthButton.color = colorRed;
    }

    public void ClickCancelCreateAvatar()
    {
        avatarSaved = false;
        persistence.avatarChoice = null;
        ProfileButtonOnClick();
    }



    public void ClickCancelChangesCreateProfileName()
    {
        persistence.avatarChoice = null;
        avatarSaved = false;

        CreateNewPlayerButtonOnClick();
    }

    public void OpenNewProfileNameGrade()
    {
        PanelCreateAvatar.SetActive(false);
        SubPanelCreateNameYearPanel.SetActive(true);

        PageText.text = "Create Profile";
        NewprofileName.text = "";

        try
        {
            if (persistence.avatarChoice != "" && avatarSaved)
            {
                NewprofileAvatar.GetComponent<Image>().sprite = Resources.Load<Sprite>("avatars/" + persistence.avatarChoice);
            }
            else
            {
                ShowMessage(true, "You did not choose an avatar.");
                CreateNewPlayerButtonOnClick();
            }
        }
        catch { }

        NewprofileName.ActivateInputField();
        NewprofileName.Select();
    }

    public void createGradeToThird()
    {
        updatePlayer.schoolarYear = 3;
        ThirdGradeNewProfile.GetComponent<Image>().color = colorRed;
        textThirdButton.color = colorWhite;

        ForthGradeNewProfile.GetComponent<Image>().color = colorWhite;
        textForthButton.color = colorRed;
    }

    public void createGradeToForth()
    {
        updatePlayer.schoolarYear = 4;
        ThirdGradeNewProfile.GetComponent<Image>().color = colorWhite;
        textThirdButton.color = colorRed;

        ForthGradeNewProfile.GetComponent<Image>().color = colorRed;
        textForthButton.color = colorWhite;
    }

    #endregion

    #region Message Functions
        
    public void ShowMessage(bool error, string message)
    {
        messagePanel.SetActive(true);
        backButton.gameObject.SetActive(true);

        yesButton.gameObject.SetActive(false);
        noButton.gameObject.SetActive(false);
        if (error)
        {
            messageBox.sprite = Resources.Load<Sprite>("Messages/error_box");
            messageBird.sprite = Resources.Load<Sprite>("Messages/error");
            buyButton.gameObject.SetActive(false);
        } else
        {
            messageBox.sprite = Resources.Load<Sprite>("Messages/info_box");
            messageBird.sprite = Resources.Load<Sprite>("Messages/info");
            buyButton.gameObject.SetActive(true);
        }
        

        messageText.text = message;
    }

    public void ShowMessageDeletePlayer(int id, GameObject go)
    {
        playerIdToDelete = id;
        objectToRemove = go;

        messagePanel.SetActive(true);

        messageBox.sprite = Resources.Load<Sprite>("Messages/error_box");
        messageBird.sprite = Resources.Load<Sprite>("Messages/error");
        messageText.text = "Do you really want to remove the player?";

        buyButton.gameObject.SetActive(false);
        backButton.gameObject.SetActive(false);

        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);

    }


    public void MessageBackButton()
    {
        messagePanel.SetActive(false);
    }

    public void MessageBuyButton()
    {
        messagePanel.SetActive(false);

        GameObject objetoShop = panelMessageInfo.objetoShop;
        persistence.CurrentPlayer.coins -= panelMessageInfo.custoItem;
        Image[] listaImagens = objetoShop.GetComponentsInChildren<Image>();
        listaImagens[1].GetComponent<Button>().enabled = true;
        objetoShop.GetComponentsInChildren<Button>().Last().gameObject.SetActive(false);

        SetPlayerInfo();
        persistence.UpdatePlayer();
        persistence.InsertItem(new ItemUnlocked(objetoShop.name,persistence.CurrentPlayer.Id));

    }

    public void YesButtonOnClick()
    {
        persistence.DeleteAllPlayerData(playerIdToDelete);
        Destroy(objectToRemove);
        messagePanel.SetActive(false);
    }

    public void NoButtonOnClick()
    {
        messagePanel.SetActive(false);
    }

    #endregion

    #region Auxiliar Functions

    private void SetMenuButtonsInteractable(bool v)
    {
        profileBtn.interactable = v;
        gamesBtn.interactable = v;
        vocabularyBtn.interactable = v;
        settingsBtn.interactable = v;
    }

    private void ClearObjects()
    {
        foreach (GameObject gameObject in objects)
        {
            Destroy(gameObject);
        }

        objects.Clear();
    }

    private void ResetListaObjetos(List<GameObject> listaObjetos)
    {
        foreach (GameObject objeto in listaObjetos)
        {
            Destroy(objeto);
        }

        listaObjetos.Clear();
    }

    public AudioSource GetAudioSource()
    {
        return audioSource;
    }

    private void ButtonClickSound()
    {
        audioSource.PlayOneShot((AudioClip)Resources.Load("Audio/Play"));
    }

    #endregion

}
