﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemToDragShop : MonoBehaviour, IEndDragHandler, IDragHandler, IBeginDragHandler
{
    private Vector3 startPosition;
    [SerializeField] Image imageItem;
    private Sprite sprite;
    private Image avatarImage;
    private Persistence persistence = Persistence.Instance;
    private String ObjectName;
    private static Vector3 initscale = Vector3.one;
    private Vector3 initialScaleToReset;
    GameObject parentAntigo;

    private float xx = Screen.width;
    private float yy = Screen.height;
    private float scale;



    public void OnBeginDrag(PointerEventData eventData)
    {
        startPosition = transform.position;
        initialScaleToReset = transform.localScale;
        if(transform.localScale.Equals(initscale))
        {
         Scale(ObjectName);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        Button botaoToRemove = transform.GetComponent<Button>();

        if (botaoToRemove != null)
        {
            botaoToRemove.interactable = false;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (avatarImage.GetComponent<BoxCollider2D>().OverlapPoint(Input.mousePosition))
        {
            transform.position = Input.mousePosition;

            if (transform.parent.name != avatarImage.name)
            {
                parentAntigo = transform.parent.parent.gameObject;
                parentAntigo.SetActive(false);
            }
            transform.parent = avatarImage.transform;


            Button botao = transform.GetComponent<Button>();
            if (botao != null)
            {
                botao.interactable = true;
            }
            else
            {
                Button button = (Button)gameObject.AddComponent(typeof(Button));
                button.onClick.AddListener(delegate { RemoveOnClick(gameObject); });
            }

            ItemUnlocked itemMoved = persistence.GetItemByName(ObjectName, persistence.CurrentPlayer.Id);

            if (itemMoved == null)
            {
                throw new Exception("Item não encontrado");
            }
            itemMoved.nameImage = imageItem.name;
            itemMoved.isBeingUsed = true;
            itemMoved.positionX = Input.mousePosition.x;
            itemMoved.positionY = Input.mousePosition.y;

            ItemToDragShop[] ListItems = avatarImage.GetComponentsInChildren<ItemToDragShop>();
            int count = 0;
            count = ListItems.Where(x => x.ObjectName.Equals(ObjectName)).Count();
            if (count > 1)
            {
                foreach (ItemToDragShop item in ListItems)
                {
                    if ( item.ObjectName == ObjectName)
                    {
                        RemoveOnDrag(item);
                        break;
                    }
                }
            }

            persistence.UpdateItem(itemMoved);

        }
        else
        {
            transform.position = startPosition;
            transform.localScale = initialScaleToReset;
            Button botaoToRemove = transform.GetComponent<Button>();
            if (botaoToRemove != null)
            {
                botaoToRemove.interactable = true;
            }
        }
    }

    private void ResetItem(ItemUnlocked toReset)
    {
        toReset.isBeingUsed = false;
        toReset.nameImage = null;
        toReset.positionX = 0;
        toReset.positionY = 0;
        persistence.UpdateItem(toReset);
    }

    public void Setup(int i, Image dollImage,string ObjectNameCategory)
    {
        sprite = Resources.Load<Sprite>("items/"+ObjectNameCategory+"/"+ ObjectNameCategory + i);
       
        if (sprite != null)
        {
            imageItem.sprite = sprite;
            imageItem.name = sprite.name;
            avatarImage = dollImage;
            ObjectName = ObjectNameCategory;

            transform.parent.name = ObjectName;
        }
    }

    public void SetupOnAvatarImage(ItemUnlocked objetoNoAvatar, Image dollImage)
    {

        sprite = Resources.Load<Sprite>("items/" + objetoNoAvatar.nameObject + "/" + objetoNoAvatar.nameImage);

        if (sprite != null)
        {

            ObjectName = objetoNoAvatar.nameObject;

            imageItem.transform.position = new Vector3(objetoNoAvatar.positionX, objetoNoAvatar.positionY, 0);
            imageItem.sprite = sprite;
            imageItem.name = sprite.name;
            avatarImage = dollImage;
            ObjectName = objetoNoAvatar.nameObject;

            Scale(ObjectName);

            Button button = (Button)gameObject.AddComponent(typeof(Button));
            button.onClick.AddListener(delegate { RemoveOnClick(gameObject); });
        }
    }

    public void RemoveOnDrag(ItemToDragShop objeto)
    {
        ItemUnlocked toReset = persistence.GetItemByName(objeto.ObjectName, persistence.CurrentPlayer.Id);
        ResetItem(toReset);
        Destroy(objeto.gameObject);
    }

    public void RemoveOnClick(GameObject objeto)
    {
        ItemUnlocked toReset = persistence.GetItemByImageName(persistence.CurrentPlayer.Id, imageItem.name);
        Destroy(objeto);
        ResetItem(toReset);
    }

    public void Scale(String ObjectName)
    {
        scale = xx * 0.0003f + 2.05f + yy * 0.0002f;
        if (xx == 1080 && yy == 1920)
        {
            scale = 2.5f;
        }


        switch (ObjectName)
        {
            case "laco":
                transform.localScale = initscale * 0.9f;
                break;
            case "auscultadores":
                transform.localScale = initscale * scale;
                break;
            case "bandolete_":
                transform.localScale = initscale * (scale -0.39f);
                break;
            case "gancho":
                transform.localScale = initscale * 0.7f;
                break;
            case "mala":
                transform.localScale = initscale * 1.85f;
                break;
            case "mochila":
                transform.localScale = initscale * 2.25f;
                break;
            case "oclulos_engracados":
                transform.localScale = initscale * 1.50f;
                break;
            case "oculos":
                transform.localScale = initscale * 1.45f;
                break;
            default:
                break;
        }
    }
}
