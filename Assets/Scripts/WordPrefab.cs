﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordPrefab : MonoBehaviour
{
    [SerializeField] private Text wordText;
    [SerializeField] private Text palavraText;
    [SerializeField] private Text exampleText;
    private Word word;
    private MenuController menuController;


    public void Setup(Word word, MenuController menuController)
    {
        this.word = word;
        this.menuController = menuController;

        wordText.text = word.word.ToLower();
        palavraText.text = word.palavra.ToLower();
        exampleText.text = word.example;
    }

    public void BtnOnClick()
    {
        AudioSource audio = menuController.GetAudioSource();
        AudioClip clip = (AudioClip) Resources.Load("Audio/Games/" + word.categoryId + "/" + word.word.ToLower());
        audio.PlayOneShot(clip);
    }
}
