﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProfileAvatar : MonoBehaviour
{
    [SerializeField] Image avatar;
    [SerializeField] Button background;
    private Sprite sprite;
    private MenuController mc;

    public void Setup(int i, MenuController mc)
    {
        sprite = Resources.Load<Sprite>("avatars/avt" + i);
        this.mc = mc;
    
        if (sprite != null)
        {
            avatar.sprite = sprite;
            avatar.name = sprite.name;
        }
    }

    public void SetupEdit(int i, MenuController mc)
    {

        background.GetComponent<Image>().sprite = Resources.Load<Sprite>("Profile/Year_Select");

        Setup(i, mc);
    }

    public void AvatarOnClick()
    {
        mc.ClearAvatarSelection();
        Persistence.Instance.avatarChoice = avatar.name;
        background.GetComponent<Image>().sprite = Resources.Load<Sprite>("Profile/Year_Select");
    }

    public void Deselect()
    {
        background.GetComponent<Image>().sprite = Resources.Load<Sprite>("Profile/Profile_Icons");
    }
}
